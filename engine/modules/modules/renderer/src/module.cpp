#include <renderer/module.h>

#include <platform/graphics/context.h>
#include <platform/graphics/renderstate.h>

namespace eg {

  RendererModule::RendererModule( void ) : Module( "renderer" ) {}

  RendererModule::~RendererModule( void ) {}

  void RendererModule::startup( void ) {
    GraphicsContext::create_instance( Window::get() );
    RenderState::create_instance( GraphicsContext::get() );

    Viewport vp   = {};
    vp.top_left_x = 0;
    vp.top_left_y = 0;
    vp.width      = Window::get()->get_x_dim();
    vp.height     = Window::get()->get_y_dim();
    vp.min_depth  = 0.0f;
    vp.max_depth  = 0.0f;

    RenderState::ref().push_topology( Topology::TRIANGLES );
    RenderState::ref().push_viewport( vp );
  }

  void RendererModule::shutdown( void ) {
    RenderState::destory_instance();
    GraphicsContext::destory_instance();
  }

  void RendererModule::on_event( Event& e ) {
  }

  void RendererModule::on_update( float delta_time ) {
  }

  void RendererModule::pre_update( void ) {
    GraphicsContext::ref().clear_buffer();
  }

  void RendererModule::post_update( void ) {
    GraphicsContext::ref().swap_buffers();
  }

}
