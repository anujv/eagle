#pragma once

#include <base/singleton.h>

namespace eg {

  class Renderer2D : public Singleton<Renderer2D> {
    REGISTER_SINGLETON_CLASS( Renderer2D );
  protected:
    Renderer2D( void );
    ~Renderer2D( void );
  private:

  };

}

