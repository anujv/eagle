#pragma once

#include <core/module.h>

namespace eg {

  class RendererModule : public Module {
  public:
    RendererModule( void );
    ~RendererModule( void );
  protected:
    virtual void startup( void ) override;
    virtual void shutdown( void ) override;
  public:
    virtual void on_event( Event& e ) override;
    virtual void on_update( float delta_time ) override;
    virtual void pre_update( void ) override;
    virtual void post_update( void ) override;
  };

}
