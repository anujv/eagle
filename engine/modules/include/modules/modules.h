#pragma once

#include <vector>

#include <base/singleton.h>
#include <core/module.h>

namespace eg {

  class Modules final : public Singleton<Modules> {
    REGISTER_SINGLETON_CLASS( Modules );
  protected:
    Modules( void ) {}
    ~Modules( void ) {}
  public:
    void register_modules( void );
    void deregister_modules( void );
  private:
    std::vector<Ref<Module>> _modules;
  };

}
