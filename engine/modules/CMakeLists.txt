
project( modules )

file( GLOB_RECURSE project_source_files src/*.* )
file( GLOB_RECURSE project_header_files include/*.* )

add_library( modules STATIC ${project_source_files} ${project_header_files} )

target_include_directories( modules PRIVATE "src" "include" ${COMMON_INCLUDE_DIRS} )
target_include_directories( modules PRIVATE ${CORE_INCLUDE_DIR} ${BASE_INCLUDE_DIR} ${PLATFORM_INCLUDE_DIR} )

target_link_libraries( modules PRIVATE core )

if( CMAKE_BUILD_TYPE STREQUAL "Debug" )
  add_compile_definitions( EG_DEBUG DEBUG )
elseif( CMAKE_BUILD_TYPE STREQUAL "Release" )
  add_compile_definitions( EG_RELEASE NDEBUG )
endif()

add_compile_definitions(
  "EG_MODULE_LOGGING_NAME=\"modules\""
)

# ------------------------ MODULES ------------------------- #

set( MOD_RENDERER_INCLUDE_DIR "modules/renderer/include" )

if ( NOT IS_ABSOLUTE ${MOD_RENDERER_INCLUDE_DIR} )
  get_filename_component( MOD_RENDERER_INCLUDE_DIR "${CMAKE_CURRENT_LIST_DIR}/${MOD_RENDERER_INCLUDE_DIR}" ABSOLUTE )
endif()

set( MODULE_INCLUDE_DIRS
  "${MOD_RENDERER_INCLUDE_DIR}"
)

set( ENABLED_MODULES
  renderer
)

add_compile_definitions( "EG_MODULE_RENDERER" )

add_subdirectory( modules/renderer )

# ---------------------------------------------------------- #

target_link_libraries( modules PRIVATE ${ENABLED_MODULES} )
target_include_directories( modules PRIVATE ${MODULE_INCLUDE_DIRS} )
