#include <modules/modules.h>

#include <core/core.h>
#include <renderer/module.h>

namespace eg {

  void Modules::register_modules( void ) {
    Ref<Module> mod;

  #ifdef EG_MODULE_RENDERER
    mod = new RendererModule();
    Core::ref().add_module( mod );
    _modules.push_back( mod );
  #endif

    mod = nullptr;
  }

  void Modules::deregister_modules(void) {

    for ( auto mod : _modules ) {
      Core::ref().remove_module( mod );
    }

    _modules.clear();
  }

}
