#pragma once

#include <base/event.h>
#include <platform/common/mousecodes.h>

#include "platform_events.h"

namespace eg {

  class MousePressedEvent : public Event {
  public:
    MousePressedEvent( MouseCode mouse_code ) :
      _mouse_code( mouse_code ) {}
    REGISTER_EVENT( MOUSE_PRESSED_EVENT )
  public:
    inline MouseCode mouse_code( void ) const { return _mouse_code; }
  private:
    MouseCode _mouse_code;
  };

  class MouseReleasedEvent : public Event {
  public:
    MouseReleasedEvent( MouseCode mouse_code ) :
      _mouse_code( mouse_code ) {}
    REGISTER_EVENT( MOUSE_RELEASED_EVENT )
  public:
    inline MouseCode mouse_code( void ) const { return _mouse_code; }
  private:
    MouseCode _mouse_code;
  };

  class MouseMovedEvent : public Event {
  public:
    MouseMovedEvent( egdouble x_pos, egdouble y_pos ) :
      _x_pos( x_pos ), _y_pos( y_pos ) {}
    REGISTER_EVENT( MOUSE_MOVED_EVENT )
  public:
    inline egdouble x( void ) const { return _x_pos; }
    inline egdouble y( void ) const { return _y_pos; }
  private:
    egdouble _x_pos;
    egdouble _y_pos;
  };

  class MouseScrolledEvent : public Event {
  public:
    MouseScrolledEvent( egdouble x_scroll, egdouble y_scroll ) :
      _x_scroll( x_scroll ), _y_scroll( y_scroll ) {}
    REGISTER_EVENT( MOUSE_SCROLLED_EVENT )
  public:
    inline egdouble x( void ) const { return _x_scroll; }
    inline egdouble y( void ) const { return _y_scroll; }
  private:
    egdouble _x_scroll;
    egdouble _y_scroll;
  };

}
