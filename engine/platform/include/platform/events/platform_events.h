#pragma once

namespace eg {

  enum PlatformEvents {
    /* window events */
    WINDOW_CLOSE_EVENT    = 0x00,
    WINDOW_RESIZE_EVENT   = 0x01,
    WINDOW_MOVE_EVENT     = 0x02,

    /* keyboard events */
    KEY_PRESSED_EVENT     = 0x03,
    KEY_RELEASED_EVENT    = 0x04,
    KEY_TYPED_EVENT       = 0x05,

    /* mouse events */
    MOUSE_PRESSED_EVENT   = 0x06,
    MOUSE_RELEASED_EVENT  = 0x07,
    MOUSE_MOVED_EVENT     = 0x08,
    MOUSE_SCROLLED_EVENT  = 0x09
  };

}
