#pragma once

#include <base/event.h>

#include "platform_events.h"

namespace eg {

  class WindowCloseEvent : public Event {
  public:
    WindowCloseEvent( bool should_close ) :
      _should_close( should_close ) {}
    REGISTER_EVENT( WINDOW_CLOSE_EVENT );
  public:
    inline bool close( void ) const { return _should_close; }
  private:
    bool _should_close;
  };

  class WindowResizeEvent : public Event {
  public:
    WindowResizeEvent( uint32_t x_dim, uint32_t y_dim ) :
      _x_dim( x_dim ), _y_dim( y_dim ) {}
    REGISTER_EVENT( WINDOW_RESIZE_EVENT );
  public:
    inline uint32_t x( void ) const { return _x_dim; }
    inline uint32_t y( void ) const { return _y_dim; }
  private:
    uint32_t _x_dim;
    uint32_t _y_dim;
  };

  class WindowMoveEvent : public Event {
  public:
    WindowMoveEvent( uint32_t x_pos, uint32_t y_pos ):
      _x_pos( x_pos ), _y_pos( y_pos ) {}
    REGISTER_EVENT( WINDOW_MOVE_EVENT );
  public:
    inline uint32_t x( void ) const { return _x_pos; }
    inline uint32_t y( void ) const { return _y_pos; }
  private:
    uint32_t _x_pos;
    uint32_t _y_pos;
  };

}
