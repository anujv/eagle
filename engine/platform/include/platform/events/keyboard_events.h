#pragma once

#include <base/event.h>
#include <platform/common/keycodes.h>

#include "platform_events.h"

namespace eg {

  class KeyPressedEvent : public Event {
  public:
    KeyPressedEvent( KeyCode key_code, int32_t repeat_count ) :
      _key_code( key_code ), _repeat_count( repeat_count ) {}
    REGISTER_EVENT( KEY_PRESSED_EVENT )
  public:
    inline int32_t repeat_count( void ) const { return _repeat_count; }
    inline KeyCode key_code(void) const { return _key_code; }
  private:
    KeyCode _key_code;
    int32_t _repeat_count;
  };

  class KeyReleasedEvent : public Event {
  public:
    KeyReleasedEvent( KeyCode key_code ) :
      _key_code( key_code ) {}
    REGISTER_EVENT( KEY_RELEASED_EVENT )
  public:
    inline KeyCode key_code(void) const { return _key_code; }
  private:
    KeyCode _key_code;
  };

  class KeyTypedEvent : public Event {
  public:
    KeyTypedEvent( uint32_t character ) :
      _charcode( character ) {}
    REGISTER_EVENT( KEY_TYPED_EVENT )
  public:
    inline uint32_t charcode( void ) const { return _charcode; }
  private:
    uint32_t _charcode;
  };

}
