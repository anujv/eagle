#pragma once

#include <stack>

#include "context.h"

namespace eg {

  enum class Topology {
    INVALID,
    POINTS,
    LINES,
    LINE_STRIP,
    LINES_ADJ,
    LINE_STRIP_ADJ,
    TRIANGLES,
    TRIANGLE_STRIP,
    TRIANGLES_ADJ,
    TRIANGLE_STRIP_ADJ
  };

  struct Viewport {
    uint32_t top_left_x, top_left_y;
    uint32_t width, height;
    float    min_depth, max_depth;
  };

  class RenderState : public Singleton<RenderState> {
    REGISTER_SINGLETON_CLASS( RenderState );
  protected:
    RenderState( void ) {}
    virtual ~RenderState( void ) {}
  public:
    virtual void draw( uint32_t count ) const = 0;

    void push_topology( Topology topology );
    void pop_topology( void );
    
    void push_viewport( Viewport viewport );
    void pop_viewport( void );
  protected:
    virtual void set_topology( Topology topology ) = 0;
    virtual void set_viewport( Viewport viewport ) = 0;
  private:
    std::stack<Topology> _topologies;
    std::stack<Viewport> _viewports;
  private:
    static RenderState* create( GraphicsContext* context );
    DECLARE_OBJECT_CREATE_FUNCTION( static RenderState*, create( GraphicsContext* context ) );
  };

  template<>
  template<typename... Args>
  inline void Singleton<RenderState>::create_instance( Args... args ) {
    if ( !s_instance )
      s_instance = RenderState::create( args... );
  }

}
