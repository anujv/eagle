#pragma once

#include <vector>

namespace eg {

  enum class GraphicsAPI {
    OPENGL,
    DIRECTX,
    VULKAN,
    METAL
  };

  std::vector<GraphicsAPI> get_available_graphics_api( void );

  #ifdef EG_GAPI_OPENGL
    #define IF_OPENGL( X ) X
  #else
    #define IF_OPENGL( X )
  #endif

  #ifdef EG_GAPI_DIRECTX
    #define IF_DIRECTX( X ) X
  #else
    #define IF_DIRECTX( X )
  #endif

  #ifdef EG_GAPI_VULKAN
    #define IF_VULKAN( X ) X
  #else
    #define IF_VULKAN( X )
  #endif

  #ifdef EG_GAPI_METAL
    #define IF_METAL( X ) X
  #else
    #define IF_METAL( X )
  #endif

  #define DECLARE_OPENGL_OBJECT_CREATE_FUNCTION(  R, F ) IF_OPENGL ( R opengl_##F  )
  #define DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( R, F ) IF_DIRECTX( R directx_##F )
  #define DECLARE_VULKAN_OBJECT_CREATE_FUNCTION(  R, F ) IF_VULKAN ( R vulkan_##F  )
  #define DECLARE_METAL_OBJECT_CREATE_FUNCTION(   R, F ) IF_METAL  ( R metal_##F  )

  #define CALL_OPENGL_OBJECT_CREATE_FUNCTION(  F, ... )  IF_OPENGL ( opengl_##F(  __VA_ARGS__ ) )
  #define CALL_DIRECTX_OBJECT_CREATE_FUNCTION( F, ... )  IF_DIRECTX( directx_##F( __VA_ARGS__ ) )
  #define CALL_VULKAN_OBJECT_CREATE_FUNCTION(  F, ... )  IF_VULKAN ( vulkan_##F(  __VA_ARGS__ ) )
  #define CALL_METAL_OBJECT_CREATE_FUNCTION(   F, ... )  IF_METAL  ( metal_##F(   __VA_ARGS__ ) )

  #define DECLARE_OBJECT_CREATE_FUNCTION( R, F )            \
            DECLARE_OPENGL_OBJECT_CREATE_FUNCTION ( R, F ); \
            DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( R, F ); \
            DECLARE_VULKAN_OBJECT_CREATE_FUNCTION ( R, F ); \
            DECLARE_METAL_OBJECT_CREATE_FUNCTION  ( R, F );

  #define DEFINE_OBJECT_CREATE_FUNCTION( A, F, ... )                                       \
              switch ( A ) {                                                               \
              case GraphicsAPI::OPENGL:                                                    \
                IF_OPENGL( val = CALL_OPENGL_OBJECT_CREATE_FUNCTION( F, __VA_ARGS__ ) );   \
                break;                                                                     \
              case GraphicsAPI::DIRECTX:                                                   \
                IF_DIRECTX( val = CALL_DIRECTX_OBJECT_CREATE_FUNCTION( F, __VA_ARGS__ ) ); \
                break;                                                                     \
              case GraphicsAPI::VULKAN:                                                    \
                IF_VULKAN( val = CALL_VULKAN_OBJECT_CREATE_FUNCTION( F, __VA_ARGS__ ) );   \
                break;                                                                     \
              case GraphicsAPI::METAL:                                                     \
                IF_METAL( val = CALL_METAL_OBJECT_CREATE_FUNCTION( F, __VA_ARGS__ ) );     \
                break;                                                                     \
              default:                                                                     \
                break;                                                                     \
              }
}
