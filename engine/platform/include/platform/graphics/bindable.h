#pragma once

#include <base/ref.h>

namespace eg {

  class Bindable : public SharedObject {
  protected:
    Bindable( void ) {}
    virtual ~Bindable( void ) {}
  public:
    virtual void bind( void ) {}
  };

}
