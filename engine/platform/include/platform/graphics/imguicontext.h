#pragma once

#include <imgui.h>

#include <base/singleton.h>

#include "context.h"

namespace eg {

  class ImGuiContext : public Singleton<ImGuiContext> {
    REGISTER_SINGLETON_CLASS( ImGuiContext );
  protected:
    ImGuiContext( GraphicsContext* context, Window* window );
    virtual ~ImGuiContext( void );
  public:
    void begin_frame( void ) const;
    void end_frame( void ) const;
  private:
    GraphicsAPI _api;
  };

}
