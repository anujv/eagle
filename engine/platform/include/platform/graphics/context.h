#pragma once

#include <base/exception.h>
#include <base/singleton.h>
#include <base/assert.h>
#include <base/types.h>
#include <base/ref.h>

#include <platform/common/window.h>
#include <platform/common/log.h>

#include "api.h"

namespace eg {

  class GraphicsContext : public Singleton<GraphicsContext> {
    REGISTER_SINGLETON_CLASS( GraphicsContext );
  protected:
    GraphicsContext( void ) {}
    virtual ~GraphicsContext( void ) {}
  public:
    virtual void        swap_buffers ( void )           = 0;
    virtual void        clear_buffer ( void )           = 0; /* deafult framebuffer */
    virtual GraphicsAPI get_api      ( void ) const     = 0;

    virtual void        resize_buffer( uint32_t width, uint32_t height ) = 0;
  private:
    static GraphicsContext* create_context( Window* window );
    DECLARE_OBJECT_CREATE_FUNCTION( static GraphicsContext*, create_context( Window* window ) );
  };

  template<>
  template<typename... Args>
  inline void Singleton<GraphicsContext>::create_instance( Args... args ) {
    if ( !s_instance )
      s_instance = GraphicsContext::create_context( args... );
  }

}
