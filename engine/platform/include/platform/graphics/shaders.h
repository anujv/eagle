#pragma once

#include "bindable.h"
#include "context.h"

namespace eg {

  enum class ShaderType {
    VERTEX,
    PIXEL,
    COMPUTE,
    GEOMETRY
  };

  class Shader : public Bindable {
  protected:
    Shader( void ) {}
    virtual ~Shader( void ) {}
  public:
    virtual ShaderType type( void ) const = 0;
  public:
    static Ref<Shader> create( const string& name, const string& code, ShaderType type, const GraphicsContext* context );
    static Ref<Shader> create( const string& name, const string& code, ShaderType type );
  };

  class VertexShader : public Shader {
  protected:
    VertexShader( void ) {}
    virtual ~VertexShader( void ) {}
  public:
    virtual ShaderType type( void ) const override { return ShaderType::VERTEX; }
  public:
    static Ref<VertexShader> create( const string& name, const string& code, const GraphicsContext* context );
    static Ref<VertexShader> create( const string& name, const string& code );
  private:
    DECLARE_OBJECT_CREATE_FUNCTION( static VertexShader*, create( const string& name, const string& code, const GraphicsContext* context ) );
  };

  class PixelShader : public Shader {
  protected:
    PixelShader( void ) {}
    virtual ~PixelShader( void ) {}
  public:
    virtual ShaderType type( void ) const override { return ShaderType::PIXEL; }
  public:
    static Ref<PixelShader> create( const string& name, const string& code, const GraphicsContext* context );
    static Ref<PixelShader> create( const string& name, const string& code );
  private:
    DECLARE_OBJECT_CREATE_FUNCTION( static PixelShader*, create( const string& name, const string& code, const GraphicsContext* context ) );
  };

  class ShaderProgram : public SharedObject {
  protected:
    ShaderProgram( void ) {}
    virtual ~ShaderProgram( void ) {}
  public:
    virtual void bind( void ) const = 0;
  public:
    static Ref<ShaderProgram> create( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context );
    static Ref<ShaderProgram> create( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps );
  private:
    DECLARE_OBJECT_CREATE_FUNCTION( static ShaderProgram*, create( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context ) );
  };

}
