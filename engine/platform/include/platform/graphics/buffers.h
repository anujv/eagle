#pragma once

#include "bindable.h"
#include "context.h"

namespace eg {

  enum class BufferType {
    ARRAY,
    INDEX,
    UNIFORM,
    TEXTURE
  };

  enum class BufferUsage {
    STATIC,
    STREAM,
    DYNAMIC
  };

  enum class BufferAccess {
    DRAW,
    READ,
    COPY
  };

  struct BufferCreateInfo {
    const void*  data;   /* can be nullptr                          */
    size_t       size;   /* in bytes                                */
    size_t       stride; /* used if accessing buffer as a structure */
    BufferUsage  usage;
    BufferAccess access;
  };

  class Buffer : public Bindable {
  protected:
    Buffer( void ) {}
    virtual ~Buffer( void ) {}
  public:
    virtual BufferType type( void ) const = 0;
    virtual void bind( void ) const = 0;
    virtual void bind( uint32_t slot ) const = 0;
  public:
    static Ref<Buffer> create( const BufferCreateInfo& create_info, BufferType type, const GraphicsContext* context );
    static Ref<Buffer> create( const BufferCreateInfo& create_info, BufferType type );
  };

  class VertexBuffer : public Buffer {
  protected:
    VertexBuffer( void ) {}
    virtual ~VertexBuffer( void ) {}
  public:
    virtual BufferType type( void ) const { return BufferType::ARRAY; }
    virtual void bind( void ) const = 0;
    virtual void bind( uint32_t slot ) const = 0;
    virtual size_t stride( void ) const = 0;
  public:
    static Ref<VertexBuffer> create( const BufferCreateInfo& create_info, const GraphicsContext* context );
    static Ref<VertexBuffer> create( const BufferCreateInfo& create_info );
  private:
    DECLARE_OBJECT_CREATE_FUNCTION( static VertexBuffer*, create( const BufferCreateInfo& create_info, const GraphicsContext* context ) );
  };

}
