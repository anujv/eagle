#pragma once

#include <vector>

#include "shaders.h"
#include "buffers.h"

namespace eg {

  enum class DataType {
    INT32,
    INT32_2,
    INT32_3,
    INT32_4,
    FLOAT32,
    FLOAT32_2,
    FLOAT32_3,
    FLOAT32_4,
  };

  static size_t get_size_bytes( DataType type ) {
    switch (type) {
    case DataType::FLOAT32:
    case DataType::INT32:     return 4;
    case DataType::FLOAT32_2:
    case DataType::INT32_2:   return 8;
    case DataType::FLOAT32_3:
    case DataType::INT32_3:   return 12;
    case DataType::FLOAT32_4:
    case DataType::INT32_4:   return 16;
    default:
      EG_THROW( "invalid data type" );
    }

    return 0;
  }

  static uint32_t get_num_elements( DataType type ) {
    switch (type) {
    case DataType::FLOAT32:
    case DataType::INT32:     return 1;
    case DataType::FLOAT32_2:
    case DataType::INT32_2:   return 2;
    case DataType::FLOAT32_3:
    case DataType::INT32_3:   return 3;
    case DataType::FLOAT32_4:
    case DataType::INT32_4:   return 4;
    default:
      EG_THROW( "invalid data type" );
    }

    return 0;
  }

  struct InputLayoutDesc {
    string   semantic_name;
    DataType data_type;
    uint32_t input_buffer_slot;
    size_t   offset;
  };

  class InputLayout : public Bindable {
  protected:
    InputLayout( void ) {}
    virtual ~InputLayout( void ) {}
  public:
    virtual void bind( void ) const = 0;
  public:
    static Ref<InputLayout> create( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context );
    static Ref<InputLayout> create( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers );
  private:
    DECLARE_OBJECT_CREATE_FUNCTION( static InputLayout*, create( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context ) );
  };

}
