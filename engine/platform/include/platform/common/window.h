#pragma once

#include <base/singleton.h>
#include <base/types.h>
#include <base/event.h>

#include <platform/graphics/api.h>

namespace eg {

  struct WindowCreateInfo {
    uint32_t    x_dim;
    uint32_t    y_dim;
    string      name;
    GraphicsAPI api;
  };

  class Window : public Singleton<Window> {
    REGISTER_SINGLETON_CLASS( Window );
  protected:
    Window() {}
    virtual ~Window( void ) {}
  public:
    virtual void        poll_events( void )                         = 0;
    virtual void        bind_event_function( event_emit_func func ) = 0;

    virtual uint32_t    get_x_dim         ( void ) const            = 0;
    virtual uint32_t    get_y_dim         ( void ) const            = 0;
    virtual string      get_display_name  ( void ) const            = 0;
    virtual GraphicsAPI get_api           ( void ) const            = 0;
    virtual uintptr_t   get_native_display( void ) const            = 0;
  private:
    static Window*      create_window( const WindowCreateInfo& create_info );
  };

  template<>
  template<typename... Args>
  inline void Singleton<Window>::create_instance( Args... args ) {
    if ( !s_instance ) {
      s_instance = Window::create_window( args... );
    }
  }

}
