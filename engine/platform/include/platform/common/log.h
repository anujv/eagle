#pragma once

#include <map>
#include <mutex>

#include <base/types.h>

#pragma warning( push, 0 )
# include <spdlog/spdlog.h>
# include <spdlog/fmt/ostr.h>
#pragma warning( pop )

namespace eg {

  /* Since logging is platform dependent, I have  */
  /* added the logger in the platform layer. Even */
  /* tough `spdlog` is platform independent.      */

  class Log {
  public:
    static void init( const string& env );

    static std::shared_ptr<spdlog::logger>& get_core_logger( void );
    static std::shared_ptr<spdlog::logger>& get_client_logger( void );

    static uint16_t get_environment_value( const string& env );
  private:
    static void     create_environment_map( const string& env );
  private:
    static std::shared_ptr<spdlog::logger> s_core_logger;
    static std::shared_ptr<spdlog::logger> s_client_logger;

    static std::map<string, uint16_t>      s_module_map;
  };

  #ifdef EG_MODULE_LOGGING_NAME

  #define EG_LOG_LEVEL_OFF      0
  #define EG_LOG_LEVEL_CRITICAL 1
  #define EG_LOG_LEVEL_ERROR    2
  #define EG_LOG_LEVEL_WARN     3
  #define EG_LOG_LEVEL_INFO     4
  #define EG_LOG_LEVEL_DEBUG    5
  #define EG_LOG_LEVEL_TRACE    6

  #define EG_CORE_GET_ENV_VAL( M )                                                             \
            uint16_t env_val = ::eg::Log::get_environment_value( M );                          \
            uint16_t any_val = ::eg::Log::get_environment_value( "any" );                      \
            env_val = any_val > env_val ? any_val : env_val                                    \

  #define EG_MODULE_PRINT_PATTERN "[" EG_MODULE_LOGGING_NAME "] "

  #define EG_CORE_TRACE( ... )                                                                 \
            do {                                                                               \
              EG_CORE_GET_ENV_VAL( EG_MODULE_LOGGING_NAME );                                   \
              if ( env_val >= EG_LOG_LEVEL_TRACE )                                             \
                ::eg::Log::get_core_logger()->trace( EG_MODULE_PRINT_PATTERN __VA_ARGS__ );    \
            } while ( 0 )

  #define EG_CORE_DEBUG( ... )                                                                 \
            do {                                                                               \
              EG_CORE_GET_ENV_VAL( EG_MODULE_LOGGING_NAME );                                   \
              if ( env_val >= EG_LOG_LEVEL_DEBUG )                                             \
                ::eg::Log::get_core_logger()->debug( EG_MODULE_PRINT_PATTERN __VA_ARGS__ );    \
            } while ( 0 )

  #define EG_CORE_INFO( ... )                                                                  \
            do {                                                                               \
              EG_CORE_GET_ENV_VAL( EG_MODULE_LOGGING_NAME );                                   \
              if ( env_val >= EG_LOG_LEVEL_INFO )                                              \
                ::eg::Log::get_core_logger()->info( EG_MODULE_PRINT_PATTERN __VA_ARGS__ );     \
            } while ( 0 )

  #define EG_CORE_WARN( ... )                                                                  \
            do {                                                                               \
              EG_CORE_GET_ENV_VAL( EG_MODULE_LOGGING_NAME );                                   \
              if ( env_val >= EG_LOG_LEVEL_WARN )                                              \
                ::eg::Log::get_core_logger()->warn( EG_MODULE_PRINT_PATTERN __VA_ARGS__ );     \
            } while ( 0 )

  #define EG_CORE_ERROR( ... )                                                                 \
            do {                                                                               \
              EG_CORE_GET_ENV_VAL( EG_MODULE_LOGGING_NAME );                                   \
              if ( env_val >= EG_LOG_LEVEL_ERROR )                                             \
                ::eg::Log::get_core_logger()->error( EG_MODULE_PRINT_PATTERN __VA_ARGS__ );    \
            } while ( 0 )

  #define EG_CORE_CRITICAL( ... )                                                              \
            do {                                                                               \
              EG_CORE_GET_ENV_VAL( EG_MODULE_LOGGING_NAME );                                   \
              if ( env_val >= EG_LOG_LEVEL_CRITICAL )                                          \
                ::eg::Log::get_core_logger()->critical( EG_MODULE_PRINT_PATTERN __VA_ARGS__ ); \
            } while ( 0 )

  #else
    /* logging macros for engine */
    #define EG_CORE_TRACE( ... )    ::eg::Log::get_core_logger()->trace( __VA_ARGS__ )
    #define EG_CORE_DEBUG( ... )    ::eg::Log::get_core_logger()->debug( __VA_ARGS__ )
    #define EG_CORE_INFO( ... )     ::eg::Log::get_core_logger()->info( __VA_ARGS__ )
    #define EG_CORE_WARN( ... )     ::eg::Log::get_core_logger()->warn( __VA_ARGS__ )
    #define EG_CORE_ERROR( ... )    ::eg::Log::get_core_logger()->error( __VA_ARGS__ )
    #define EG_CORE_CRITICAL( ... ) ::eg::Log::get_core_logger()->critical( __VA_ARGS__ )
  #endif

  /* logging macros for client */
  #define EG_LOG_TRACE( ... )       ::eg::Log::get_client_logger()->trace( __VA_ARGS__ )
  #define EG_LOG_DEBUG( ... )       ::eg::Log::get_client_logger()->debug( __VA_ARGS__ )
  #define EG_LOG_INFO( ... )        ::eg::Log::get_client_logger()->info( __VA_ARGS__ )
  #define EG_LOG_WARN( ... )        ::eg::Log::get_client_logger()->warn( __VA_ARGS__ )
  #define EG_LOG_ERROR( ... )       ::eg::Log::get_client_logger()->error( __VA_ARGS__ )
  #define EG_LOG_CRITICAL( ... )    ::eg::Log::get_client_logger()->critical( __VA_ARGS__ )

}
