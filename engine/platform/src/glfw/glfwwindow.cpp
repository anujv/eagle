#include "glfwwindow.h"

#include <platform/events/window_events.h>
#include <platform/events/keyboard_events.h>
#include <platform/events/mouse_events.h>

namespace eg {

  static void window_close_callback( ::GLFWwindow* window               );
  static void window_move_callback(  ::GLFWwindow* window, int x, int y );
  static void window_size_callback(  ::GLFWwindow* window, int x, int y );
  static void key_callback(          ::GLFWwindow* window, int key, int scancode, int action, int mods );
  static void char_callback(         ::GLFWwindow* window, unsigned int charcode );
  static void cursor_pos_callback(   ::GLFWwindow* window, double x, double y );
  static void mouse_button_callback( ::GLFWwindow* window, int button, int action, int mods );
  static void mouse_scroll_callback( ::GLFWwindow* window, double xoffset, double yoffset );

  Window* Window::create_window( const WindowCreateInfo& create_info ) {
    return dynamic_cast<Window*>( new GLFWWindow( create_info ) );
  }

  static void glfw_error_callback( int error, const char* desc ) {
    EG_CORE_ERROR( "glfw error - {}", desc );
  }

  GLFWWindow::GLFWWindow( const WindowCreateInfo& create_info ) :
    _window( nullptr ) {

    EG_CORE_DEBUG( "creating window of size: [{}, {}] and name: `{}`", create_info.x_dim, create_info.y_dim, create_info.name );

    glfwSetErrorCallback( glfw_error_callback );
    
    if ( glfwInit() != GLFW_TRUE )
      EG_THROW( "failed to initialize glfw" );

    if ( create_info.api == GraphicsAPI::OPENGL ) {
      glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
      glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 6 );
      glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
    } else {
      glfwWindowHint( GLFW_CLIENT_API, GLFW_NO_API );
    }
    _window = glfwCreateWindow( create_info.x_dim, create_info.y_dim, create_info.name.c_str(), NULL, NULL );

    if ( !_window )
      EG_THROW( "failed to create glfw window" );

    /* set callback functions */

    /* window */
    glfwSetWindowUserPointer  ( _window, this                  );
    glfwSetWindowCloseCallback( _window, window_close_callback );
    glfwSetWindowPosCallback  ( _window, window_move_callback  );
    glfwSetWindowSizeCallback ( _window, window_size_callback  );

    /* mouse */
    glfwSetCursorPosCallback  ( _window, cursor_pos_callback   );
    glfwSetMouseButtonCallback( _window, mouse_button_callback );
    glfwSetScrollCallback     ( _window, mouse_scroll_callback );

    /* keyboard */
    glfwSetKeyCallback        ( _window, key_callback          );
    glfwSetCharCallback       ( _window, char_callback         );

    EG_CORE_DEBUG( "glfw window created successfully" );

    _info = create_info;
  }

  GLFWWindow::~GLFWWindow( void ) {
    glfwDestroyWindow( _window );
    glfwTerminate();

    EG_CORE_DEBUG( "glfw terminated and window destroyed successfully" );
  }

  void GLFWWindow::poll_events( void ) {
    glfwPollEvents();
  }

  void GLFWWindow::bind_event_function( event_emit_func func ) {
    if ( func )
      _event_func = func;

    EG_CORE_DEBUG( "window event emit function updated" );
  }

  /* glfw callback functions */

  event_emit_func get_emit_func( ::GLFWwindow* window ) {
    GLFWWindow* user_pointer = (GLFWWindow*)glfwGetWindowUserPointer(window);

    if (!user_pointer) {
      EG_CORE_ERROR("invalid user pointer; set the user pointer properly to get event callbacks");
      return nullptr;
    }

    event_emit_func func = user_pointer->get_event_function();

    if (!func) {
      EG_CORE_WARN("the window has events but no callback function has been provided");
      return nullptr;
    }

    return func;
  }

  /* window callbacks */

  void window_close_callback( ::GLFWwindow* window ) {
    event_emit_func func = get_emit_func( window );

    if ( !func )
      return;

    WindowCloseEvent e( true );
    func( e );
  }

  void window_move_callback( ::GLFWwindow* window, int x, int y ) {
    event_emit_func func = get_emit_func( window );

    if ( !func )
      return;

    WindowMoveEvent e( (uint32_t)x, (uint32_t)y );
    func( e );
  }

  void window_size_callback( ::GLFWwindow* window, int x, int y ) {
    event_emit_func func = get_emit_func( window );

    if ( !func )
      return;

    WindowResizeEvent e( (uint32_t)x, (uint32_t)y );
    func( e );
  }

  /* key callbacks */

  void key_callback( ::GLFWwindow* window, int key, int scancode, int action, int mods ) {
    event_emit_func func = get_emit_func( window );

    if ( !func )
      return;

    switch ( action ) {
    case GLFW_PRESS:
    case GLFW_REPEAT:
    {
      KeyPressedEvent e( key, action == GLFW_REPEAT );
      func( e );
      break;
    }
    case GLFW_RELEASE:
    {
      KeyReleasedEvent e( key );
      func( e );
      break;
    }
    default:
      EG_CORE_WARN( "invlid glfw key action - {}", action );
      break;
    }
  }

  void char_callback( ::GLFWwindow* window, unsigned int charcode ) {
    event_emit_func func = get_emit_func( window );

    if ( !func )
      return;

    KeyTypedEvent e( charcode );
    func( e );
  }

  /* mouse callbacks */

  static void cursor_pos_callback( ::GLFWwindow* window, double x, double y ) {
    event_emit_func func = get_emit_func( window );

    if ( !func )
      return;

    MouseMovedEvent e( x, y );
    func( e );
  }

  static void mouse_button_callback( ::GLFWwindow* window, int button, int action, int mods ) {
    event_emit_func func = get_emit_func( window );

    if ( !func )
      return;

    switch ( action )
    {
    case GLFW_PRESS:
    {
      MousePressedEvent e( button );
      func( e );
      break;
    }
    case GLFW_RELEASE:
    {
      MouseReleasedEvent e( button );
      func( e );
      break;
    }
    default:
      EG_CORE_WARN( "invlid glfw mouse action - {}", action );
      break;
    }
  }

  static void mouse_scroll_callback( ::GLFWwindow* window, double xoffset, double yoffset ) {
    event_emit_func func = get_emit_func( window );

    if ( !func )
      return;

    MouseScrolledEvent e( xoffset, yoffset );
    func( e );
  }

}