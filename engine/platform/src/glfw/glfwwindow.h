#pragma once

#include <base/exception.h>

#include <platform/common/window.h>
#include <platform/common/log.h>

#include <GLFW/glfw3.h>

namespace eg {

  class GLFWWindow final : public Window {
  public:
    GLFWWindow( const WindowCreateInfo& create_info );
    ~GLFWWindow( void );
  public:
    virtual void        poll_events( void ) override;
    virtual void        bind_event_function( event_emit_func func ) override;

    virtual uint32_t    get_x_dim         ( void ) const override { return _info.x_dim; }
    virtual uint32_t    get_y_dim         ( void ) const override { return _info.y_dim; }
    virtual string      get_display_name  ( void ) const override { return _info.name; }
    virtual GraphicsAPI get_api           ( void ) const override { return _info.api; }
    virtual uintptr_t   get_native_display( void ) const override { return (uintptr_t)_window; }
  public:
    event_emit_func     get_event_function( void ) const { return _event_func; }
  private:
    ::GLFWwindow*    _window;
    WindowCreateInfo _info;
    event_emit_func  _event_func;
  };

}
