#include <platform/common/log.h>

#include <vector>
#include <sstream>
#include <cstdlib>

#pragma warning( push, 0 )
#  include <spdlog/sinks/stdout_color_sinks.h>
#  include <spdlog/sinks/basic_file_sink.h>
#pragma warning( pop )

namespace eg {

  std::shared_ptr<spdlog::logger> Log::s_core_logger   = nullptr;
  std::shared_ptr<spdlog::logger> Log::s_client_logger = nullptr;
  std::map<string, uint16_t>      Log::s_module_map;

  void Log::init( const string& env ) {
    std::vector<spdlog::sink_ptr> log_sinks;
    log_sinks.emplace_back( std::make_shared<spdlog::sinks::stdout_color_sink_mt>() );

    log_sinks[0]->set_pattern( "%^[%=3L] %n: %v%$" );

    s_core_logger = std::make_shared<spdlog::logger>( "ENG", log_sinks.begin(), log_sinks.end() );
    spdlog::register_logger( s_core_logger );
    s_core_logger->set_level( spdlog::level::trace );
    s_core_logger->flush_on( spdlog::level::trace );

    s_client_logger = std::make_shared<spdlog::logger>( "APP", log_sinks.begin(), log_sinks.end() );
    spdlog::register_logger( s_client_logger );
    s_client_logger->set_level( spdlog::level::trace );
    s_client_logger->flush_on( spdlog::level::trace );

    create_environment_map( env );
  }

  std::shared_ptr<spdlog::logger>& Log::get_core_logger( void ) {
    return s_core_logger;
  }

  std::shared_ptr<spdlog::logger>& Log::get_client_logger( void ) {
    return s_client_logger;
  }

  void Log::create_environment_map( const string& env ) {

    std::istringstream iss( env );
    string             word;

    while( iss >> word ) {

      string left  = word.substr( 0, word.find_first_of( ':' ) );
      string right = word.substr( word.find_first_of( ':' ) + 1 );

      uint16_t val = std::atoi( right.c_str() );

      s_module_map.insert( std::make_pair( left, val ) );
    }

  }

  uint16_t Log::get_environment_value( const string& env ) {
    auto it = s_module_map.find( env );

    if ( it == s_module_map.end() )
      return 0;

    return it->second;
  }

}