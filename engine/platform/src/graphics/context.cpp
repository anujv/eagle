#include <platform/graphics/context.h>

namespace eg {

  GraphicsContext* GraphicsContext::create_context( Window* window ) {

    EG_ASSERT( window  );

    GraphicsContext* val = nullptr;
    GraphicsAPI      api = window->get_api();

    DEFINE_OBJECT_CREATE_FUNCTION( api, create_context, window );

    if ( !val )
      EG_THROW( "no valid graphics api detected; either the platform does not support any api or you did not define/pass a valid graphics API" );

    return val;
  }

}