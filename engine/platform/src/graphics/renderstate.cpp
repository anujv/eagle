#include <platform/graphics/renderstate.h>

namespace eg {

  RenderState* RenderState::create( GraphicsContext* context ) {
    
    EG_ASSERT( context );

    RenderState* val = nullptr;
    GraphicsAPI  api = context->get_api();

    DEFINE_OBJECT_CREATE_FUNCTION( api, create, context );

    if ( !val )
      EG_THROW( "failed to create render state" );

    return val;
  }

  void RenderState::push_topology( Topology topology ) {
    _topologies.push( topology );
    set_topology( topology );
  }

  void RenderState::pop_topology( void ) {
    if ( _topologies.size() > 0 ) {
      _topologies.pop();
      set_topology( _topologies.top() );
    } else {
      set_topology( Topology::INVALID );
    }
  }

  void RenderState::push_viewport( Viewport viewport ) {
    _viewports.push( viewport );
    set_viewport( viewport );
  }

  void RenderState::pop_viewport( void ) {
    if ( _viewports.size() > 0 ) {
      _viewports.pop();
      set_viewport( _viewports.top() );
    } else {
      Viewport vp = { 0, 0, 0, 0, 0.0f, 0.0f };
      set_viewport( vp );
    }
  }

}