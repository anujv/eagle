#include <platform/graphics/api.h>

namespace eg {

  std::vector<GraphicsAPI> get_available_graphics_api( void ) {
    /* currently we only check weather the specific API is defined */

    std::vector<GraphicsAPI> available_apis;

  #ifdef EG_GAPI_OPENGL
    available_apis.push_back( GraphicsAPI::OPENGL );
  #endif
  #ifdef EG_GAPI_DIRECTX
    available_apis.push_back( GraphicsAPI::DIRECTX );
  #endif
  #ifdef EG_GAPI_VULKAN
    available_apis.push_back( GraphicsAPI::VULKAN );
  #endif
  #ifdef EG_GAPI_METAL
    available_apis.push_back( GraphicsAPI::METAL );
  #endif

    return available_apis;
  }

}