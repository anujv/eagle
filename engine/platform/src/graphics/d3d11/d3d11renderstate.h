#pragma once

#ifdef EG_GAPI_DIRECTX

#include <platform/graphics/renderstate.h>

#include "d3d11context.h"

namespace eg {

  class D3DRenderState final : public RenderState {
  public:
    D3DRenderState( GraphicsContext* context );
    ~D3DRenderState( void );
  public:
    virtual void set_topology( Topology topology ) override;
    virtual void set_viewport( Viewport viewport ) override;
    virtual void draw( uint32_t count ) const override;
  private:
    D3DContext*            _context;
    D3D_PRIMITIVE_TOPOLOGY _topology;
  };

}

#endif /* EG_GAPI_DIRECTX */
