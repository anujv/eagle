#include "d3d11shaders.h"

#ifdef EG_GAPI_DIRECTX

#include "typemap.h"

#define VERTEX_SHADER_MODEL "vs_5_0"
#define PIXEL_SHADER_MODEL  "ps_5_0"

namespace eg {

  DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( VertexShader* VertexShader::, create( const string& name, const string& code, const GraphicsContext* context ) ) {
    return new D3DVertexShader( name, code, context );
  }

  D3DVertexShader::D3DVertexShader( const string& name, const string& code, const GraphicsContext* context ) {
    EG_ASSERT( context );
    const D3DContext* con = dynamic_cast<const D3DContext*>( context );
    EG_ASSERT( con );

    UINT flags = 0u;

  #ifdef EG_DEBUG
    flags |= D3DCOMPILE_DEBUG;
  #endif

    ComPtr<ID3DBlob> error_messages;

    HRESULT hr = D3DCompile(
      code.c_str(),
      code.size(),
      nullptr,
      nullptr,
      nullptr,
      "main",
      VERTEX_SHADER_MODEL,
      flags,
      0u,
      _bytecode.GetAddressOf(),
      error_messages.GetAddressOf()
     );

    if ( FAILED(hr) ) {
      string err = (const char*)error_messages->GetBufferPointer();
      EG_THROW_SPECIFIC( EGShaderCompilationException, err, "directx - failed to create vertex shader" );
    }

    hr = con->device()->CreateVertexShader(
      _bytecode->GetBufferPointer(),
      _bytecode->GetBufferSize(),
      nullptr, _shader.GetAddressOf()
    );

    THROW_IF_FAILED( hr, "directx - failed to create vertex shader" );

    _context = con->device_context();

    EG_CORE_DEBUG( "created directx vertex shader with name = {}", name );
  }

  D3DVertexShader::~D3DVertexShader( void ) {
    _shader = nullptr;
    _bytecode = nullptr;
    EG_CORE_DEBUG( "deleted directx vertex shader" );
  }

  void D3DVertexShader::bind( void ) const {
    _context->VSSetShader( _shader.Get(), nullptr, 0u );
  }

  DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( PixelShader* PixelShader::, create( const string& name, const string& code, const GraphicsContext* context ) ) {
    return new D3DPixelShader( name, code, context );
  }

  D3DPixelShader::D3DPixelShader( const string& name, const string& code, const GraphicsContext* context ) {
    EG_ASSERT( context );
    const D3DContext* con = dynamic_cast<const D3DContext*>( context );
    EG_ASSERT( con );

    UINT flags = 0u;

  #ifdef EG_DEBUG
    flags |= D3DCOMPILE_DEBUG;
  #endif

    ComPtr<ID3DBlob> error_messages;

    HRESULT hr = D3DCompile(
      code.c_str(),
      code.size(),
      nullptr,
      nullptr,
      nullptr,
      "main",
      PIXEL_SHADER_MODEL,
      flags,
      0u,
      _bytecode.GetAddressOf(),
      error_messages.GetAddressOf()
     );

    if ( FAILED( hr ) ) {
      string err = (const char*)error_messages->GetBufferPointer();
      EG_THROW_SPECIFIC( EGShaderCompilationException, err, "directx - failed to create pixel shader" );
    }

    hr = con->device()->CreatePixelShader(
      _bytecode->GetBufferPointer(),
      _bytecode->GetBufferSize(),
      nullptr, _shader.GetAddressOf()
    );

    THROW_IF_FAILED( hr, "directx - failed to create pixel shader" );

    _context = con->device_context();

    EG_CORE_DEBUG( "created directx pixel shader with name = {}", name );
  }

  D3DPixelShader::~D3DPixelShader( void ) {
    _shader = nullptr;
    _bytecode = nullptr;
    EG_CORE_DEBUG( "deleted directx pixel shader" );
  }

  void D3DPixelShader::bind( void ) const {
    _context->PSSetShader( _shader.Get(), nullptr, 0u );
  }

  DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( ShaderProgram* ShaderProgram::, create( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context ) ) {
    return new D3DShaderProgram( vs, ps, context );
  }

  D3DShaderProgram::D3DShaderProgram( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context ) {
    EG_ASSERT( vs && ps && context );
    _vs = vs;
    _ps = ps;
    EG_CORE_DEBUG( "created directx shader program" );
  }

  D3DShaderProgram::~D3DShaderProgram( void ) {
    _vs = nullptr;
    _ps = nullptr;
    EG_CORE_DEBUG( "deleted directx shader program" );
  }

  void D3DShaderProgram::bind( void ) const {
    _vs->bind();
    _ps->bind();
  }

}

#endif /* EG_GAPI_DIRECTX */
