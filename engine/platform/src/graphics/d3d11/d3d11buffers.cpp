#include "d3d11buffers.h"

#ifdef EG_GAPI_DIRECTX

#include "typemap.h"

namespace eg {

  const static UINT s_offset = 0;

  DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( VertexBuffer* VertexBuffer::, create( const BufferCreateInfo& create_info, const GraphicsContext* context ) ) {
    return new D3DVertexBuffer( create_info, context );
  }

  D3DVertexBuffer::D3DVertexBuffer( const BufferCreateInfo& create_info, const GraphicsContext* context ) {
    EG_ASSERT( context );
    const D3DContext* con = dynamic_cast<const D3DContext*>( context );
    EG_ASSERT( con );

    UINT cpu_access = 0u;

    switch ( create_info.access ) {
    case BufferAccess::READ:
      cpu_access = D3D11_CPU_ACCESS_READ;
      break;
    case BufferAccess::COPY:
      cpu_access = D3D11_CPU_ACCESS_WRITE;
      break;
    default:
      break;
    }

    if ( create_info.usage == BufferUsage::STATIC )
      EG_ASSERT( create_info.data );

    D3D11_BUFFER_DESC vbd;

    vbd.ByteWidth           = create_info.size;
    vbd.BindFlags           = D3D11_BIND_VERTEX_BUFFER;
    vbd.CPUAccessFlags      = cpu_access;
    vbd.MiscFlags           = 0u;
    vbd.StructureByteStride = create_info.stride;
    vbd.Usage               = map( create_info.usage );

    HRESULT hr;

    if ( create_info.data != nullptr ) {
      D3D11_SUBRESOURCE_DATA vbsd;

      vbsd.pSysMem          = create_info.data;
      vbsd.SysMemPitch      = 0u;
      vbsd.SysMemSlicePitch = 0u;

      hr = con->device()->CreateBuffer( &vbd, &vbsd, _buffer.GetAddressOf() );
    } else {
      hr = con->device()->CreateBuffer( &vbd, nullptr, _buffer.GetAddressOf() );
    }

    THROW_IF_FAILED( hr, "directx - failed to create vertex buffer" );

    _context = con->device_context();
    _buffer_stride = create_info.stride;

    EG_CORE_DEBUG( "created directx vertex buffer" );
  }

  D3DVertexBuffer::~D3DVertexBuffer( void ) {
    _buffer = nullptr;
    EG_CORE_DEBUG( "deleted directx vertex buffer" );
  }

  void D3DVertexBuffer::bind( void ) const {
    _context->IASetVertexBuffers( 0, 1, _buffer.GetAddressOf(), (const UINT*)&_buffer_stride, &s_offset );
  }

  void D3DVertexBuffer::bind( uint32_t slot ) const {
    _context->IASetVertexBuffers( slot, 1, _buffer.GetAddressOf(), (const UINT*)&_buffer_stride, &s_offset );
  }

}

#endif /* EG_GAPI_DIRECTX */
