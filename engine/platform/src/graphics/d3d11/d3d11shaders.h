#pragma once

#ifdef EG_GAPI_DIRECTX

#include <platform/graphics/shaders.h>

#include "d3d11context.h"

namespace eg {

  class D3DVertexShader final : public VertexShader {
  public:
    D3DVertexShader( const string& name, const string& code, const GraphicsContext* context );
    ~D3DVertexShader( void );
  public:
    void bind( void ) const;

    inline ComPtr<ID3D11VertexShader> get( void ) const { return _shader; }
    inline ComPtr<ID3DBlob> bytecode( void ) const { return _bytecode; }
  private:
    ComPtr<ID3D11VertexShader>  _shader;
    ComPtr<ID3DBlob>            _bytecode;
    ComPtr<ID3D11DeviceContext> _context;
  };

  class D3DPixelShader final : public PixelShader {
  public:
    D3DPixelShader( const string& name, const string& code, const GraphicsContext* context );
    ~D3DPixelShader( void );
  public:
    void bind( void ) const;

    inline ComPtr<ID3D11PixelShader> get( void ) const { return _shader; }
    inline ComPtr<ID3DBlob> bytecode( void ) const { return _bytecode; }
  private:
    ComPtr<ID3D11PixelShader>   _shader;
    ComPtr<ID3DBlob>            _bytecode;
    ComPtr<ID3D11DeviceContext> _context;
  };

  class D3DShaderProgram final : public ShaderProgram {
  public:
    D3DShaderProgram( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context );
    ~D3DShaderProgram( void );
  public:
    virtual void bind( void ) const override;

    inline Ref<D3DVertexShader> vs( void ) const { return _vs; }
    inline Ref<D3DPixelShader>  ps( void ) const { return _ps; }
  private:
    Ref<D3DVertexShader> _vs;
    Ref<D3DPixelShader>  _ps;
  };

}

#endif /* #ifdef EG_GAPI_DIRECTX */
