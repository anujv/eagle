#include "d3d11inputlayout.h"

#ifdef EG_GAPI_DIRECTX

#include "typemap.h"

namespace eg {

  DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( InputLayout* InputLayout::, create( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context ) ) {
    return new D3DInputLayout( shader_program, input_desc, buffers, context );
  }

  D3DInputLayout::D3DInputLayout( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context ) {
    EG_ASSERT( context );
    EG_ASSERT( input_desc.size() > 0 && buffers.size() > 0 );
    const D3DContext* con = dynamic_cast<const D3DContext*>( context );
    EG_ASSERT( con );

    Ref<D3DShaderProgram> sp = shader_program;

    EG_ASSERT( sp );

    size_t max_input_slots = 0;

    for ( const auto& layout : input_desc ) {
      if ( layout.input_buffer_slot > max_input_slots )
        max_input_slots = layout.input_buffer_slot;
    }

    EG_ASSERT( buffers.size() == max_input_slots + 1 );

    std::vector<InputLayoutDesc> sid = input_desc; /* sorted input desc */

    std::sort( sid.begin(), sid.end(),
      []( const InputLayoutDesc& lhs, const InputLayoutDesc& rhs ) -> bool {
        return lhs.input_buffer_slot < rhs.input_buffer_slot;
      }
    );

    std::vector<D3D11_INPUT_ELEMENT_DESC> ieds( sid.size() );

    for ( size_t i = 0; i < ieds.size(); i++ ) {
      D3D11_INPUT_ELEMENT_DESC& ied = ieds[i];

      ied.SemanticName         = sid[i].semantic_name.c_str();
      ied.SemanticIndex        = 0u;
      ied.Format               = map( sid[i].data_type );
      ied.InputSlot            = sid[i].input_buffer_slot;
      ied.AlignedByteOffset    = sid[i].offset;
      ied.InputSlotClass       = D3D11_INPUT_PER_VERTEX_DATA;
      ied.InstanceDataStepRate = 0u;
    }

    Ref<D3DVertexShader> vs = sp->vs();

    HRESULT hr = con->device()->CreateInputLayout( ieds.data(), (UINT)ieds.size(),
      vs->bytecode()->GetBufferPointer(), vs->bytecode()->GetBufferSize(),
      _input_layout.GetAddressOf() );

    THROW_IF_FAILED( hr, "directx - failed to create input layout" );

    _context = con->device_context();
    _buffers = buffers;

    EG_CORE_DEBUG( "created directx input layout" );
  }

  D3DInputLayout::~D3DInputLayout( void ) {
    _input_layout = nullptr;
    _context = nullptr;
    EG_CORE_DEBUG( "deleted directx input layout" );
  }

  void D3DInputLayout::bind( void ) const {
    int32_t slot = 0;

    _context->IASetInputLayout( _input_layout.Get() );
    for ( const auto& buff : _buffers ) {
      buff->bind( slot );
      slot++;
    }
  }

}

#endif /* EG_GAPI_DIRECTX */
