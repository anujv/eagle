#include "d3d11renderstate.h"

#ifdef EG_GAPI_DIRECTX

#include "typemap.h"

namespace eg {

  DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( RenderState* RenderState::, create( GraphicsContext* context ) ) {
    return new D3DRenderState( context );
  }

  D3DRenderState::D3DRenderState( GraphicsContext* context ) {
    _context = dynamic_cast<D3DContext*>( context );
    EG_ASSERT( _context );

    _topology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;

    EG_CORE_DEBUG( "created directx render state" );
  }

  D3DRenderState::~D3DRenderState( void ) {
    EG_CORE_DEBUG( "deleted directx render state" );
  }

  void D3DRenderState::set_topology( Topology topology ) {
    _topology = map( topology );
    _context->device_context()->IASetPrimitiveTopology( _topology );
  }

  void D3DRenderState::set_viewport( Viewport viewport ) {
    D3D11_VIEWPORT vp;
    vp.TopLeftX = (FLOAT)viewport.top_left_x;
    vp.TopLeftY = (FLOAT)viewport.top_left_y;
    vp.Width    = (FLOAT)viewport.width;
    vp.Height   = (FLOAT)viewport.height;
    vp.MinDepth = viewport.min_depth;
    vp.MaxDepth = viewport.max_depth;

    _context->device_context()->RSSetViewports( 1, &vp );
  }

  void D3DRenderState::draw( uint32_t count ) const {
    _context->device_context()->Draw( (UINT)count, 0u );
  }

}

#endif /* EG_GAPI_DIRECTX */
