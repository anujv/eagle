#pragma once

#include <d3d11.h>

#include <platform/graphics/buffers.h>
#include <platform/graphics/inputlayout.h>
#include <platform/graphics/renderstate.h>

namespace eg {

  static D3D11_USAGE map( BufferUsage usage ) {
    switch ( usage ) {
    case BufferUsage::STATIC:  return D3D11_USAGE_IMMUTABLE;
    case BufferUsage::STREAM:  return D3D11_USAGE_DYNAMIC;
    case BufferUsage::DYNAMIC: return D3D11_USAGE_DYNAMIC;
    default:
      break;
    }

    EG_THROW( "invalid buffer usage" );
    return (D3D11_USAGE)-1;
  }

  static DXGI_FORMAT map( DataType type ) {
    switch (type) {
    case eg::DataType::INT32:     return DXGI_FORMAT_R32_SINT;
    case eg::DataType::INT32_2:   return DXGI_FORMAT_R32G32_SINT;
    case eg::DataType::INT32_3:   return DXGI_FORMAT_R32G32B32_SINT;
    case eg::DataType::INT32_4:   return DXGI_FORMAT_R32G32B32A32_SINT;
    case eg::DataType::FLOAT32:   return DXGI_FORMAT_R32_FLOAT;
    case eg::DataType::FLOAT32_2: return DXGI_FORMAT_R32G32_FLOAT;
    case eg::DataType::FLOAT32_3: return DXGI_FORMAT_R32G32B32_FLOAT;
    case eg::DataType::FLOAT32_4: return DXGI_FORMAT_R32G32B32A32_FLOAT;
    default:
      break;
    }

    EG_THROW( "invalid data type" );
    return (DXGI_FORMAT)-1;
  }

  static D3D_PRIMITIVE_TOPOLOGY map( Topology topology ) {
    switch (topology) {
    case eg::Topology::INVALID:            return D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
    case eg::Topology::POINTS:             return D3D11_PRIMITIVE_TOPOLOGY_POINTLIST;
    case eg::Topology::LINES:              return D3D11_PRIMITIVE_TOPOLOGY_LINELIST;
    case eg::Topology::LINE_STRIP:         return D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP;
    case eg::Topology::LINES_ADJ:          return D3D11_PRIMITIVE_TOPOLOGY_LINELIST_ADJ;
    case eg::Topology::LINE_STRIP_ADJ:     return D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ;
    case eg::Topology::TRIANGLES:			     return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
    case eg::Topology::TRIANGLE_STRIP:     return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
    case eg::Topology::TRIANGLES_ADJ:      return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST_ADJ;
    case eg::Topology::TRIANGLE_STRIP_ADJ: return D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP_ADJ;
    default:
      break;
    }

    EG_THROW( "invalid primitive topology" );
    return D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
  }

}
