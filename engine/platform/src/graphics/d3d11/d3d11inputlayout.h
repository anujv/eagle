#pragma once

#ifdef EG_GAPI_DIRECTX

#include <platform/graphics/inputlayout.h>

#include "d3d11context.h"
#include "d3d11shaders.h"

namespace eg {

  class D3DInputLayout final : public InputLayout {
  public:
    D3DInputLayout( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context );
    ~D3DInputLayout( void );
  public:
    void bind( void ) const override;
  private:
    ComPtr<ID3D11InputLayout>      _input_layout;
    ComPtr<ID3D11DeviceContext>    _context;
    std::vector<Ref<VertexBuffer>> _buffers;
  };

}


#endif /* #ifdef EG_GAPI_DIRECTX */
