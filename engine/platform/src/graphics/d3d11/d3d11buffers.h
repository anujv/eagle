#pragma once

#ifdef EG_GAPI_DIRECTX

#include <platform/graphics/buffers.h>

#include "d3d11context.h"

namespace eg {

  class D3DVertexBuffer final : public VertexBuffer {
  public:
    D3DVertexBuffer( const BufferCreateInfo& create_info, const GraphicsContext* context );
    ~D3DVertexBuffer( void );
  public:
    virtual void bind( void ) const override;
    virtual void bind( uint32_t slot ) const override;

    virtual size_t stride( void ) const override { return _buffer_stride; }
  private:
    ComPtr<ID3D11Buffer>        _buffer;
    ComPtr<ID3D11DeviceContext> _context;
    size_t                      _buffer_size;
    size_t                      _buffer_stride;
  };

}


#endif /* EG_GAPI_DIRECTX */
