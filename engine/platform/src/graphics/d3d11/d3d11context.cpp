#include "d3d11context.h"

#ifdef EG_GAPI_DIRECTX

#include <vector>

namespace eg {

  DECLARE_DIRECTX_OBJECT_CREATE_FUNCTION( GraphicsContext* GraphicsContext::, create_context( Window* window ) ) {
    return new D3DContext( window );
  }

  D3DContext::D3DContext( Window* window ) {
    EG_CORE_DEBUG( "creating directx context" );
    EG_ASSERT( window );

    _window = (::GLFWwindow*)window->get_native_display();
    _hwnd   = glfwGetWin32Window( _window );

    ComPtr<IDXGIAdapter> adapter = get_default_adapter();
    DXGI_RATIONAL        refresh_rate = get_refresh_rate( adapter );

    DXGI_MODE_DESC bd;
    ZeroMemory( &bd, sizeof( bd ) );

    bd.Format           = DXGI_FORMAT_R8G8B8A8_UNORM;
    bd.RefreshRate      = refresh_rate;
    bd.Scaling          = DXGI_MODE_SCALING_UNSPECIFIED;
    bd.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    bd.Width            = (UINT)window->get_x_dim();
    bd.Height           = (UINT)window->get_y_dim();

    create_device_and_swapchain( adapter, bd );
    create_render_target_view();
    create_depth_stencil_view( bd );
    create_rasterizer_state();

    EG_CORE_DEBUG( "directx context created successfully" );
  }

  D3DContext::~D3DContext( void ) {
    EG_CORE_DEBUG( "directx context destroyed" );
  }

  void D3DContext::swap_buffers( void ) {
    _swap_chain->Present( 1u, 0u );
  }

  void D3DContext::clear_buffer( void ) {
    float clearColor[] = { 0.0f, 0.0f, 0.0f, 0.0f };
    _device_context->ClearRenderTargetView( _render_target_view.Get(), clearColor );
    _device_context->ClearDepthStencilView( _depth_stencil_view.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0 );
  }

  void D3DContext::resize_buffer( uint32_t width, uint32_t height ) {
    width = width;
    height = height;
    /* [TODO] */
  }

  ComPtr<IDXGIAdapter> D3DContext::get_default_adapter( void ) const {

    ComPtr<IDXGIFactory> factory;
    ComPtr<IDXGIAdapter> adapter;

    HRESULT hr = CreateDXGIFactory( __uuidof( IDXGIFactory ), (void **)factory.GetAddressOf() );
    THROW_IF_FAILED( hr, "failed to create directx factory" );

    hr = factory->EnumAdapters( 0, adapter.GetAddressOf() );
    THROW_IF_FAILED( hr, "failed to enumerate directx adapters" );

    return adapter;
  }

  DXGI_RATIONAL D3DContext::get_refresh_rate(ComPtr<IDXGIAdapter> adapter) const {
    UINT num_modes;

    ComPtr<IDXGIOutput> output;

    HRESULT hr = adapter->EnumOutputs( 0, output.GetAddressOf() );
    THROW_IF_FAILED( hr, "failed to get directx adapter output" );

    hr = output->GetDisplayModeList( DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &num_modes, nullptr );
    THROW_IF_FAILED( hr, "failed to get directx adapter display mode list" );

    std::vector<DXGI_MODE_DESC> display_mode_list( num_modes );

    hr = output->GetDisplayModeList( DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &num_modes, display_mode_list.data() );
    THROW_IF_FAILED( hr, "failed to get directx adapter display mode list" );

    return display_mode_list.back().RefreshRate;
  }

  void D3DContext::create_device_and_swapchain( ComPtr<IDXGIAdapter> adapter, DXGI_MODE_DESC bd ) {

    DXGI_SWAP_CHAIN_DESC scd;
    ZeroMemory( &scd, sizeof( scd ) );

    scd.BufferDesc         = bd;
    scd.SampleDesc.Count   = 1;
    scd.SampleDesc.Quality = 0;
    scd.BufferUsage        = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    scd.BufferCount        = 1;
    scd.OutputWindow       = _hwnd;
    scd.Windowed           = TRUE;
    scd.SwapEffect         = DXGI_SWAP_EFFECT_DISCARD;
    scd.Flags              = 0;

    D3D_FEATURE_LEVEL feature_levels[] = {
      D3D_FEATURE_LEVEL_11_1,
      D3D_FEATURE_LEVEL_11_0
    };

    D3D_FEATURE_LEVEL selected_feature_level;

    UINT flags = 0;

  #ifdef EG_DEBUG
    flags |= D3D11_CREATE_DEVICE_DEBUG;
  #endif

    HRESULT hr = D3D11CreateDeviceAndSwapChain( adapter.Get(), D3D_DRIVER_TYPE_UNKNOWN,
      NULL, flags, feature_levels, std::size( feature_levels ), D3D11_SDK_VERSION, &scd,
      _swap_chain.GetAddressOf(), _device.GetAddressOf(), &selected_feature_level, _device_context.GetAddressOf() );

    THROW_IF_FAILED( hr, "failed to create directx device and swap chain" );
  }

  void D3DContext::create_render_target_view( void ) {
    ComPtr<ID3D11Resource> back_buffer;
    HRESULT hr = _swap_chain->GetBuffer( 0, __uuidof( ID3D11Resource ), (void**)back_buffer.GetAddressOf() );
    THROW_IF_FAILED( hr, "directx - failed to get back buffer" );
    hr = _device->CreateRenderTargetView( back_buffer.Get(), NULL, _render_target_view.GetAddressOf() );
    THROW_IF_FAILED( hr, "directx - failed to create render target view" );

    _device_context->OMSetRenderTargets( 1, _render_target_view.GetAddressOf(), NULL );
  }

  void D3DContext::create_depth_stencil_view( DXGI_MODE_DESC bd ) {
    D3D11_DEPTH_STENCIL_DESC dsd;
    ZeroMemory( &dsd, sizeof( dsd ) );

    dsd.DepthEnable    = TRUE;
    dsd.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    dsd.DepthFunc      = D3D11_COMPARISON_LESS_EQUAL;
    dsd.StencilEnable  = FALSE;

    HRESULT hr = _device->CreateDepthStencilState( &dsd, _depth_stencil_state.GetAddressOf() );
    THROW_IF_FAILED( hr, "directx - failed to create depth stencil state" );

    _device_context->OMSetDepthStencilState( _depth_stencil_state.Get(), 1u );

    /* move depth stencil to render state */

    D3D11_TEXTURE2D_DESC dbd;
    ZeroMemory( &dbd, sizeof( dbd ) );

    dbd.Width              = bd.Width;
    dbd.Height             = bd.Height;
    dbd.MipLevels          = 1;
    dbd.ArraySize          = 1;
    dbd.Format             = DXGI_FORMAT_D32_FLOAT;
    dbd.SampleDesc.Count   = 1;
    dbd.SampleDesc.Quality = 0;
    dbd.Usage              = D3D11_USAGE_DEFAULT;
    dbd.BindFlags          = D3D11_BIND_DEPTH_STENCIL;
    dbd.CPUAccessFlags     = 0;
    dbd.MiscFlags          = 0;

    hr = _device->CreateTexture2D( &dbd, NULL, _depth_stencil_buffer.GetAddressOf() );
    THROW_IF_FAILED( hr, "directx - failed to create depth stencil buffer" );

    D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
    ZeroMemory( &dsvd, sizeof( dsvd ) );

    dsvd.Format             = DXGI_FORMAT_D32_FLOAT;
    dsvd.ViewDimension      = D3D11_DSV_DIMENSION_TEXTURE2D;
    dsvd.Texture2D.MipSlice = 0;

    hr = _device->CreateDepthStencilView( _depth_stencil_buffer.Get(), &dsvd, _depth_stencil_view.GetAddressOf() );
    THROW_IF_FAILED( hr, "directx - failed to create depth stencil view" );

    _device_context->OMSetRenderTargets( 1u, _render_target_view.GetAddressOf(), _depth_stencil_view.Get() );
  }

  void D3DContext::create_rasterizer_state( void ) {
    D3D11_RASTERIZER_DESC rd;
    ZeroMemory( &rd, sizeof( rd ) );

    rd.FillMode              = D3D11_FILL_SOLID;
    rd.CullMode              = D3D11_CULL_NONE;
    rd.FrontCounterClockwise = FALSE;
    rd.DepthBias             = 0;
    rd.DepthBiasClamp        = 0.0f;
    rd.SlopeScaledDepthBias  = 0.0f;
    rd.DepthClipEnable       = FALSE;
    rd.ScissorEnable         = FALSE;
    rd.MultisampleEnable     = FALSE;
    rd.AntialiasedLineEnable = FALSE;

    HRESULT hr = _device->CreateRasterizerState( &rd, _rasterizer_state.GetAddressOf() );

    _device_context->RSSetState( _rasterizer_state.Get() );
  }

}

#endif /* EG_GAPI_DIRECTX */
