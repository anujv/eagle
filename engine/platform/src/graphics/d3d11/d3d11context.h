#pragma once

#ifdef EG_GAPI_DIRECTX

#pragma comment( lib, "d3d11.lib" )
#pragma comment( lib, "dxgi.lib" )
#pragma comment( lib, "d3dcompiler.lib" )

#include <wrl.h>
#include <dxgi.h>
#include <d3d11.h>
#include <d3dcompiler.h>

#define GLFW_INCLUDE_NONE
#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include <platform/graphics/context.h>

#define THROW_IF_FAILED( H, M ) if ( FAILED( hr ) ) { EG_THROW( M ); }

namespace eg {

  template<typename T>
  using ComPtr = Microsoft::WRL::ComPtr<T>;

  class D3DContext final : public GraphicsContext {
  public:
    D3DContext( Window* window );
    ~D3DContext( void );
  public:
    virtual void        swap_buffers ( void ) override;
    virtual void        clear_buffer ( void ) override;
    virtual GraphicsAPI get_api      ( void ) const override { return GraphicsAPI::DIRECTX; };

    virtual void        resize_buffer( uint32_t width, uint32_t height ) override;
  public:
    ComPtr<ID3D11Device> device( void ) const { return _device; }
    ComPtr<ID3D11DeviceContext> device_context( void ) const { return _device_context; }
  private:
    ComPtr<IDXGIAdapter> get_default_adapter( void ) const;
    DXGI_RATIONAL get_refresh_rate( ComPtr<IDXGIAdapter> adapter ) const;

    void create_device_and_swapchain( ComPtr<IDXGIAdapter> adapter, DXGI_MODE_DESC bd );
    void create_render_target_view( void );
    void create_depth_stencil_view( DXGI_MODE_DESC bd  );
    void create_rasterizer_state( void );
  private:
    ::GLFWwindow* _window;
    ::HWND        _hwnd;

    ComPtr<IDXGISwapChain>          _swap_chain;
    ComPtr<ID3D11Device>            _device;
    ComPtr<ID3D11DeviceContext>     _device_context;
    ComPtr<ID3D11RenderTargetView>  _render_target_view;
    ComPtr<ID3D11RasterizerState>   _rasterizer_state;
    ComPtr<ID3D11Texture2D>         _depth_stencil_buffer;
    ComPtr<ID3D11DepthStencilState> _depth_stencil_state;
    ComPtr<ID3D11DepthStencilView>  _depth_stencil_view;
  };

}

#endif /* EG_GAPI_DIRECTX */
