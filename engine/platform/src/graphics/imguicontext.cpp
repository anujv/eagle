#include <platform/graphics/imguicontext.h>

#include <functional>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#include <platform/common/window.h>
#include <platform/graphics/context.h>

#include <backends/imgui_impl_glfw.h>
#include <backends/imgui_impl_glfw.cpp>

#ifdef EG_GAPI_OPENGL
  #include "opengl/openglcontext.h"
  #define IMGUI_IMPL_OPENGL_LOADER_GLAD
  #include <backends/imgui_impl_opengl3.h>
  #include <backends/imgui_impl_opengl3.cpp>
#endif

#ifdef EG_GAPI_DIRECTX
  #include "d3d11/d3d11context.h"
  #include <backends/imgui_impl_dx11.h>
  #include <backends/imgui_impl_dx11.cpp>
#endif

#ifdef EG_GAPI_VULKAN
  #include <backends/imgui_impl_vulkan.h>
  #include <backends/imgui_impl_vulkan.cpp>
#endif

#ifdef EG_GAPI_METAL
  #include <backends/imgui_impl_metal.h>
  #include <backends/imgui_impl_metal.cpp>
#endif

namespace eg {

  #ifdef EG_GAPI_OPENGL
    static void opengl_init_func( GraphicsContext* context, Window* window ) {
      ::GLFWwindow* win = (::GLFWwindow*)window->get_native_display();
      EG_ASSERT( context && win );

      const char glsl_version[] = "#version 460 core";
      if ( !ImGui_ImplGlfw_InitForOpenGL( win, true ) )
        EG_THROW( "failed to initialize imgui glfw" );
      if ( !ImGui_ImplOpenGL3_Init( glsl_version ) )
        EG_THROW( "failed to initialize imgui opengl" );
    }

    static void opengl_shut_func( void ) {
      ImGui_ImplOpenGL3_Shutdown();
      ImGui_ImplGlfw_Shutdown();
    }

    static void opengl_begin_frame( void ) {
      ImGui_ImplOpenGL3_NewFrame();
      ImGui_ImplGlfw_NewFrame();
    }

    static void opengl_end_frame( void ) {
      ImGui_ImplOpenGL3_RenderDrawData( ImGui::GetDrawData() );
    }
  #endif

  #ifdef EG_GAPI_DIRECTX
    static void directx_init_func( GraphicsContext* context, Window* window ) {
      ::GLFWwindow* win = (::GLFWwindow*)window->get_native_display();
      D3DContext*   con = dynamic_cast<D3DContext*>( context );
      EG_ASSERT( win && con );

      if ( !ImGui_ImplGlfw_InitForOther( win, true ) )
        EG_THROW( "failed to initialize imgui glfw" );
      if ( !ImGui_ImplDX11_Init( con->device().Get(), con->device_context().Get() ) )
        EG_THROW( "failed to initialize imgui directx" );
    }

    static void directx_shut_func( void ) {
      ImGui_ImplDX11_Shutdown();
      ImGui_ImplGlfw_Shutdown();
    }

    static void directx_being_frame( void ) {
      ImGui_ImplDX11_NewFrame();
      ImGui_ImplGlfw_NewFrame();
    }

    static void directx_end_frame( void ) {
      ImGui_ImplDX11_RenderDrawData( ImGui::GetDrawData() );
    }
  #endif

  struct ImGuiBackendFuncs {
    std::function<void( GraphicsContext* context, Window* window )> init_func;
    std::function<void( void )>                                     shut_func;
    std::function<void( void )>                                     begin_frame_func;
    std::function<void( void )>                                     end_frame_func;
  };

  static ImGuiBackendFuncs backend_funcs;

  ImGuiContext::ImGuiContext( GraphicsContext* context, Window* window ) {
    EG_ASSERT( context && window );

    GraphicsAPI   api = context->get_api();
    ::GLFWwindow* win = (::GLFWwindow*)window->get_native_display();

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
    //io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;       // Enable Multi-Viewport / Platform Windows
    //io.ConfigViewportsNoAutoMerge = true;
    //io.ConfigViewportsNoTaskBarIcon = true;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
    ImGuiStyle& style = ImGui::GetStyle();
    if ( io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable ) {
      style.WindowRounding = 0.0f;
      style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

    switch ( api ) {
    case eg::GraphicsAPI::OPENGL:
    {
      IF_OPENGL(
        backend_funcs.init_func        = opengl_init_func;
        backend_funcs.shut_func        = opengl_shut_func;
        backend_funcs.begin_frame_func = opengl_begin_frame;
        backend_funcs.end_frame_func   = opengl_end_frame;
      )
    }
      break;
    case eg::GraphicsAPI::DIRECTX:
    {
      IF_DIRECTX(
        backend_funcs.init_func        = directx_init_func;
        backend_funcs.shut_func        = directx_shut_func;
        backend_funcs.begin_frame_func = directx_being_frame;
        backend_funcs.end_frame_func   = directx_end_frame;
      )
    }
      break;
    case eg::GraphicsAPI::VULKAN:
      break;
    case eg::GraphicsAPI::METAL:
      break;
    default:
      break;
    }

    if ( backend_funcs.init_func )
      backend_funcs.init_func( context, window );
    else
      EG_THROW( "invalid graphics api for imgui; or api not yet supported" );

    EG_CORE_DEBUG( "created imgui context" );

    _api = api;
  }

  eg::ImGuiContext::~ImGuiContext( void ) {
    backend_funcs.shut_func();
    EG_CORE_DEBUG( "delted imgui context" );
  }

  void eg::ImGuiContext::begin_frame( void ) const {
    backend_funcs.begin_frame_func();
    ImGui::NewFrame();
  }

  void eg::ImGuiContext::end_frame( void ) const {
  #ifdef EG_DEBUG
    IF_OPENGL( if ( _api == GraphicsAPI::OPENGL ) glDisable( GL_DEBUG_OUTPUT ) );
  #endif

    ImGui::Render();
    backend_funcs.end_frame_func();

  #ifdef EG_DEBUG
    IF_OPENGL( if (_api == GraphicsAPI::OPENGL) glEnable( GL_DEBUG_OUTPUT ) );
  #endif
  }


}

