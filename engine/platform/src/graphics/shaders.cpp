#include <platform/graphics/shaders.h>

namespace eg {

  Ref<Shader> Shader::create( const string& name, const string& code, ShaderType type, const GraphicsContext* context ) {

    EG_ASSERT( context );

    Ref<Shader> val;

    switch ( type ) {
    case ShaderType::VERTEX:
      val = VertexShader::create( name, code, context );
      break;
    case ShaderType::PIXEL:
      val = PixelShader::create( name, code, context );
      break;
    case ShaderType::COMPUTE:
    case ShaderType::GEOMETRY:
      EG_THROW( "shader type not yet implemented" );
      break;
    default:
      EG_THROW( "invalid shader type" );
      break;
    }

    return val;
  }

  Ref<Shader> Shader::create( const string& name, const string& code, ShaderType type ) {
    return create( name, code, type, GraphicsContext::get() );
  }

  Ref<VertexShader> VertexShader::create( const string& name, const string& code, const GraphicsContext* context ) {

    EG_ASSERT( context );

    VertexShader* val = nullptr;
    GraphicsAPI   api = context->get_api();

    DEFINE_OBJECT_CREATE_FUNCTION( api, create, name, code, context );

    if (!val) {
      EG_THROW( "failed to create vertex shader object" );
    }

    return Ref<VertexShader>( val );
  }

  Ref<VertexShader> VertexShader::create( const string& name, const string& code ) {
    return create( name, code, GraphicsContext::get() );
  }

  Ref<PixelShader> PixelShader::create( const string& name, const string& code, const GraphicsContext* context ) {

    EG_ASSERT( context );

    PixelShader* val = nullptr;
    GraphicsAPI  api = context->get_api();

    DEFINE_OBJECT_CREATE_FUNCTION( api, create, name, code, context );

    if (!val)
      EG_THROW( "failed to create vertex shader object" );

    return Ref<PixelShader>( val );
  }

  Ref<PixelShader> PixelShader::create( const string& name, const string& code ) {
    return create( name, code, GraphicsContext::get() );
  }

  Ref<ShaderProgram> ShaderProgram::create( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context ) {

    EG_ASSERT( context );

    ShaderProgram* val = nullptr;
    GraphicsAPI    api = context->get_api();

    DEFINE_OBJECT_CREATE_FUNCTION( api, create, vs, ps, context );

    if ( !val )
      EG_THROW( "failed to create shader program" );

    return Ref<ShaderProgram>( val );
  }

  Ref<ShaderProgram> ShaderProgram::create( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps ) {
    return create( vs, ps, GraphicsContext::get() );
  }

}
