#pragma once

#ifdef EG_GAPI_OPENGL

#include <platform/graphics/renderstate.h>

#include "openglcontext.h"

namespace eg {

  class OpenGLRenderState final : public RenderState {
  public:
    OpenGLRenderState( GraphicsContext* context );
    ~OpenGLRenderState( void );
  public:
    virtual void set_topology( Topology topology ) override;
    virtual void set_viewport( Viewport viewport ) override;
    virtual void draw( uint32_t count ) const override;
  private:
    OpenGLContext* _context;
    GLenum         _topology;
  };

}

#endif /* EG_GAPI_OPENGL */
