#pragma once

#ifdef EG_GAPI_OPENGL

#include <platform/graphics/shaders.h>

#include "openglcontext.h"

namespace eg {

  class OpenGLVertexShader final : public VertexShader {
  public:
    OpenGLVertexShader( const string& name, const string& code, const GraphicsContext* context );
    ~OpenGLVertexShader( void );
  public:
    inline GLuint get_id( void ) const { return _shader; }
  private:
    GLuint _shader;
    string _name;
  };

  class OpenGLPixelShader final : public PixelShader {
  public:
    OpenGLPixelShader( const string& name, const string& code, const GraphicsContext* context );
    ~OpenGLPixelShader( void );
  public:
    inline GLuint get_id( void ) const { return _shader; }
  private:
    GLuint _shader;
    string _name;
  };

  class OpenGLShaderProgram final : public ShaderProgram {
  public:
    OpenGLShaderProgram( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context );
    ~OpenGLShaderProgram( void );
  public:
    virtual void bind( void ) const override;

    GLuint get_attrib_location( const string& name ) const;
  private:
    GLuint _program;
    Ref<OpenGLVertexShader> _vertex_shader;
    Ref<OpenGLPixelShader>  _pixel_shader;
  };

}

#endif /* EG_GAPI_OPENGL */
