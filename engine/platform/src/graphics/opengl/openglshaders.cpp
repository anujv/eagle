#include "openglshaders.h"

#ifdef EG_GAPI_OPENGL

#include "typemap.h"

namespace eg {

  #define INFO_LOG_SIZE 1024

  static bool get_shader_status( GLuint id, GLenum type ) {
    EG_ASSERT( type == GL_COMPILE_STATUS || type == GL_LINK_STATUS );

    GLint    success;
    GLchar   info_log[1024];

    if ( type == GL_COMPILE_STATUS ) {
      glGetShaderiv( id, GL_COMPILE_STATUS, &success );
      if ( !success ) {
        glGetShaderInfoLog( id, INFO_LOG_SIZE, NULL, info_log );
        EG_CORE_ERROR( "opengl - failed to compile shader" );
        EG_THROW_SPECIFIC( EGShaderCompilationException, info_log, "opengl - failed to compile shader" );
      }
    } else {
      glGetProgramiv( id, GL_LINK_STATUS, &success );
      if ( !success ) {
        glGetProgramInfoLog( id, INFO_LOG_SIZE, NULL, info_log );
        EG_CORE_ERROR( "opengl - failed to link shader program" );
        EG_THROW_SPECIFIC( EGShaderCompilationException, info_log, "opengl - failed to link shader program" );
      }
    }

    return success == 0 ? false : true;
  }

  DECLARE_OPENGL_OBJECT_CREATE_FUNCTION( VertexShader* VertexShader::, create( const string& name, const string& code, const GraphicsContext* context ) ) {
    return new OpenGLVertexShader( name, code, context );
  }

  OpenGLVertexShader::OpenGLVertexShader( const string& name, const string& code, const GraphicsContext* context ) {
    EG_ASSERT( context );

    _shader = glCreateShader( GL_VERTEX_SHADER );
    if ( !_shader )
      EG_THROW( "opengl - failed to create shader object" );

    const char* shader_code_string = code.c_str();
    GLsizei     shader_code_size   = (GLsizei)code.size();

    glShaderSource( _shader, 1, &shader_code_string, &shader_code_size );
    glCompileShader( _shader );
    if ( !get_shader_status( _shader, GL_COMPILE_STATUS ) )
      return;

    EG_CORE_DEBUG( "created opengl vertex shader with id = {}", _shader );

    _name = name;
  }

  OpenGLVertexShader::~OpenGLVertexShader( void ) {
    glDeleteShader( _shader );
    EG_CORE_DEBUG( "deleted opengl vertex shader with id = {}", _shader );
  }

  DECLARE_OPENGL_OBJECT_CREATE_FUNCTION( PixelShader* PixelShader::, create( const string& name, const string& code, const GraphicsContext* context ) ) {
    return new OpenGLPixelShader( name, code, context );
  }

  OpenGLPixelShader::OpenGLPixelShader( const string& name, const string& code, const GraphicsContext* context ) {
    EG_ASSERT( context );

    _shader = glCreateShader( GL_FRAGMENT_SHADER );
    if ( !_shader )
      EG_THROW( "opengl - failed to create shader object" );

    const char* shader_code_string = code.c_str();
    GLsizei     shader_code_size   = (GLsizei)code.size();

    glShaderSource( _shader, 1, &shader_code_string, &shader_code_size );
    glCompileShader( _shader );
    if ( !get_shader_status( _shader, GL_COMPILE_STATUS ) )
      return;

    EG_CORE_DEBUG( "created opengl pixel shader with id = {}", _shader );

    _name = name;
  }

  OpenGLPixelShader::~OpenGLPixelShader( void ) {
    glDeleteShader( _shader );
    EG_CORE_DEBUG( "deleted opengl pixel shader with id = {}", _shader );
  }

  DECLARE_OPENGL_OBJECT_CREATE_FUNCTION( ShaderProgram* ShaderProgram::, create( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context ) ) {
    return new OpenGLShaderProgram( vs, ps, context );
  }

  OpenGLShaderProgram::OpenGLShaderProgram( const Ref<VertexShader>& vs, const Ref<PixelShader>& ps, const GraphicsContext* context ) {
    EG_ASSERT( context );
    EG_ASSERT( vs && ps );

    _vertex_shader = vs;
    _pixel_shader  = ps;

    _program = glCreateProgram();
    if ( !_program )
      EG_THROW( "opengl - failed to create shader program" );

    glAttachShader( _program, _vertex_shader->get_id() );
    glAttachShader( _program, _pixel_shader->get_id() );
    glLinkProgram( _program );

    if ( !get_shader_status( _program, GL_LINK_STATUS ) )
      return;

    EG_CORE_DEBUG( "created opengl shader program with id = {}", _program );
  }

  OpenGLShaderProgram::~OpenGLShaderProgram( void ) {
    glDeleteProgram( _program );
    EG_CORE_DEBUG( "deleted opengl shader program with id = {}", _program );
  }

  void OpenGLShaderProgram::bind( void ) const {
    glUseProgram( _program );
  }

  GLuint OpenGLShaderProgram::get_attrib_location( const string& name ) const {
    return glGetAttribLocation( _program, name.c_str() );
  }

}

#endif /* EG_GAPI_OPENGL */
