#pragma once

#ifdef EG_GAPI_OPENGL

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <platform/graphics/context.h>

namespace eg {

  class OpenGLContext final : public GraphicsContext {
  public:
    OpenGLContext( Window* window );
    ~OpenGLContext( void );
  public:
    virtual void        swap_buffers ( void ) override;
    virtual void        clear_buffer ( void ) override;
    virtual GraphicsAPI get_api      ( void ) const override { return GraphicsAPI::OPENGL; };

    virtual void        resize_buffer( uint32_t width, uint32_t height ) override;
  private:
    ::GLFWwindow* _native_window;
  };

}

#endif /* EG_GAPI_OPENGL */
