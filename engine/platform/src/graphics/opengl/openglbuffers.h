#pragma once

#ifdef EG_GAPI_OPENGL

#include <platform/graphics/buffers.h>

#include "openglcontext.h"

namespace eg {

  class OpenGLVertexBuffer final : public VertexBuffer {
  public:
    OpenGLVertexBuffer( const BufferCreateInfo& create_info, const GraphicsContext* context );
    ~OpenGLVertexBuffer( void );
  public:
    virtual void bind( void ) const override;
    virtual void bind( uint32_t slot ) const override {}

    virtual size_t stride( void ) const override { return _buffer_stride; }
  private:
    GLuint _buffer;
    GLenum _usage;
    size_t _buffer_size;
    size_t _buffer_stride;
  };

}

#endif /* EG_GAPI_OPENGL */
