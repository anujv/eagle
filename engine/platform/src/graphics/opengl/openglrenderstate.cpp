#include "openglrenderstate.h"

#ifdef EG_GAPI_OPENGL

#include "typemap.h"

namespace eg {

  DECLARE_OPENGL_OBJECT_CREATE_FUNCTION( RenderState* RenderState::, create( GraphicsContext* context ) ) {
    return new OpenGLRenderState( context );
  }

  OpenGLRenderState::OpenGLRenderState( GraphicsContext* context ) {
    _context = dynamic_cast<OpenGLContext*>( context );
    EG_ASSERT( _context );

    EG_CORE_DEBUG( "created opengl render state" );
  }

  OpenGLRenderState::~OpenGLRenderState( void ) {
    EG_CORE_DEBUG( "deleted opengl render state" );
  }

  void OpenGLRenderState::set_topology( Topology topology ) {
    _topology = map( topology );
  }

  void OpenGLRenderState::set_viewport( Viewport viewport ) {
    glViewport( (GLint)viewport.top_left_x, (GLint)viewport.top_left_y,
                (GLsizei)viewport.width, (GLsizei)viewport.height );
    glDepthRange( (GLdouble)viewport.min_depth, (GLdouble)viewport.max_depth );
  }

  void OpenGLRenderState::draw( uint32_t count ) const {
    glDrawArrays( _topology, 0, (GLsizei)count );
  }

}

#endif
