#pragma once

#ifdef EG_GAPI_OPENGL

#include <vector>

#include <platform/graphics/inputlayout.h>

#include "openglcontext.h"
#include "openglbuffers.h"
#include "openglshaders.h"

namespace eg {

  class OpenGLVertexArray final : public InputLayout {
  public:
    OpenGLVertexArray( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context );
    ~OpenGLVertexArray( void );
  public:
    virtual void bind( void ) const override;
  private:
    GLuint                         _vao;
    std::vector<Ref<VertexBuffer>> _buffers;
  };

}

#endif /* EG_GAPI_OPENGL */
