#include "openglbuffers.h"

#ifdef EG_GAPI_OPENGL

#include "typemap.h"

namespace eg {

  DECLARE_OPENGL_OBJECT_CREATE_FUNCTION( VertexBuffer* VertexBuffer::, create( const BufferCreateInfo& create_info, const GraphicsContext* context ) ) {
    return new OpenGLVertexBuffer( create_info, context );
  }

  OpenGLVertexBuffer::OpenGLVertexBuffer( const BufferCreateInfo& create_info, const GraphicsContext* context ) {
    EG_ASSERT( context );

    if ( create_info.usage == BufferUsage::STATIC )
      EG_ASSERT( create_info.data );

    _usage = map( create_info.usage, create_info.access );

    glCreateBuffers( 1, &_buffer );

    if ( !_buffer )
      EG_THROW( "opengl - failed to create vertex buffer" );

    glNamedBufferData( _buffer, create_info.size, create_info.data, _usage );
    EG_CORE_DEBUG( "created opengl vertex buffer with id = {}", _buffer );

    _buffer_size = create_info.size;
    _buffer_stride = create_info.stride;
  }

  OpenGLVertexBuffer::~OpenGLVertexBuffer( void ) {
    glDeleteBuffers( 1, &_buffer );
    EG_CORE_DEBUG( "deleted opegnl vertex buffer with id = {}", _buffer );
  }

  void OpenGLVertexBuffer::bind( void ) const {
    glBindBuffer( GL_ARRAY_BUFFER, _buffer );
  }

}

#endif /* EG_GAPI_OPENGL */
