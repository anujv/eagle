#include "openglcontext.h"

#include <base/exception.h>
#include <base/assert.h>

#ifdef EG_GAPI_OPENGL

namespace eg {

  static void GLAPIENTRY opengl_callback(
    GLenum        source,
    GLenum        type,
    GLuint        id,
    GLenum        severity,
    GLsizei       length,
    const GLchar* message,
    const void*   user_param ) {
  
    const char* source_string = nullptr;
    const char* type_string   = nullptr;

#define GL_CASE_ENUM_TO_STRING( E, V ) case E: V = #E; break

    switch ( source ) {
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_SOURCE_API,             source_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_SOURCE_WINDOW_SYSTEM,   source_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_SOURCE_SHADER_COMPILER, source_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_SOURCE_THIRD_PARTY,     source_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_SOURCE_APPLICATION,     source_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_SOURCE_OTHER,           source_string );
    default:
      EG_LOG_WARN( "opengl - invalid source in debug callback {}", source );
      break;
    }

    switch ( type ) {
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_ERROR,               type_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, type_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR,  type_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_PORTABILITY,         type_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_PERFORMANCE,         type_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_MARKER,              type_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_PUSH_GROUP,          type_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_POP_GROUP,           type_string );
    GL_CASE_ENUM_TO_STRING( GL_DEBUG_TYPE_OTHER,               type_string );
    default:
      EG_LOG_WARN( "opengl - invalid type in debug callback {}", type );
      break;
    }
    
    const size_t        line_length         = 50;
    size_t              current_line_length = 0;
    size_t              prev_index          = 0;
    string              message_string      = message;
    std::vector<string> lines;

    for ( size_t i = 0; i < message_string.size(); i++ ) {

      if ( current_line_length > line_length &&
           ( message_string[i] == ' ' || i == message_string.size() - 1 ) ) {
        lines.push_back( message_string.substr( prev_index, current_line_length ) );
        current_line_length = 0;
        prev_index = i + 1;
      }

      current_line_length++;
    }

  #define LOG_ERROR( F ) F( "opengl - error callback [source = {}, type = {}, id = {}]", source_string, type_string, id ); \
                         for ( auto line : lines ) F( "         {}", line )

    switch ( severity ) {
    case GL_DEBUG_SEVERITY_HIGH:         LOG_ERROR( EG_CORE_CRITICAL ); break;
    case GL_DEBUG_SEVERITY_MEDIUM:       LOG_ERROR( EG_CORE_ERROR    ); break;
    case GL_DEBUG_SEVERITY_LOW:          LOG_ERROR( EG_CORE_WARN     ); break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: LOG_ERROR( EG_CORE_INFO     ); break;
    default:                             LOG_ERROR( EG_CORE_INFO     ); break;
    }

  }

  DECLARE_OPENGL_OBJECT_CREATE_FUNCTION( GraphicsContext* GraphicsContext::, create_context( Window* window ) ) {
    return new OpenGLContext( window );
  }

  OpenGLContext::OpenGLContext( Window* window ) {
    EG_CORE_DEBUG( "creating opengl context" );
    EG_ASSERT( window != nullptr );

    ::GLFWwindow* glfw_window = (::GLFWwindow*)window->get_native_display();

    EG_ASSERT( glfw_window );
    _native_window = glfw_window;

    glfwMakeContextCurrent( glfw_window );

    if ( !gladLoadGLLoader( (GLADloadproc)glfwGetProcAddress ) )
      EG_THROW( "failed to load opengl function addresses | failed to initialize glad" );

  #ifdef EG_DEBUG
    glEnable( GL_DEBUG_OUTPUT );
    glDebugMessageCallback( opengl_callback, nullptr );
  #endif

    glfwSwapInterval( 1 );
    resize_buffer( (int32_t)window->get_x_dim(), (int32_t)window->get_y_dim() );

    glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );

    EG_CORE_DEBUG( "opengl context created successfully" );
    EG_CORE_DEBUG( "  Version : {}", glGetString( GL_VERSION  ) );
    EG_CORE_DEBUG( "  Renderer: {}", glGetString( GL_RENDERER ) );
    EG_CORE_DEBUG( "  Vendor  : {}", glGetString( GL_VENDOR   ) );
  }

  OpenGLContext::~OpenGLContext( void ) {
    EG_CORE_DEBUG( "opengl context destroyed" );
  }

  void OpenGLContext::swap_buffers( void ) {
    glfwSwapBuffers( _native_window );
  }

  void OpenGLContext::clear_buffer( void ) {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  }

  void OpenGLContext::resize_buffer( uint32_t width, uint32_t height ) {
    EG_ASSERT( width != 0 && height != 0 );

    glViewport( 0, 0, width, height );

    EG_CORE_DEBUG( "default framebuffer size updated - [{}, {}]", width, height );
  }

}

#endif /* EG_GAPI_OPENGL */
