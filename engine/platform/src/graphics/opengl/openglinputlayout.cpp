#include "openglinputlayout.h"

#ifdef EG_GAPI_OPENGL

#include "typemap.h"

namespace eg {

  DECLARE_OPENGL_OBJECT_CREATE_FUNCTION( InputLayout* InputLayout::, create( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context ) ) {
    return new OpenGLVertexArray(shader_program, input_desc, buffers, context );
  }

  OpenGLVertexArray::OpenGLVertexArray( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context ) {
    EG_ASSERT( context );
    EG_ASSERT( input_desc.size() > 0 && buffers.size() > 0 );

    Ref<OpenGLShaderProgram> sp = shader_program;

    EG_ASSERT( sp );

    glCreateVertexArrays( 1, &_vao );

    if ( !_vao )
      EG_THROW( "opengl - failed to create vertex array" );

    size_t max_input_slots = 0;

    for ( const auto& layout : input_desc ) {
      if ( layout.input_buffer_slot > max_input_slots )
        max_input_slots = layout.input_buffer_slot;
    }

    EG_ASSERT( buffers.size() == max_input_slots + 1 );

    std::vector<InputLayoutDesc> sid = input_desc; /* sorted input desc */

    std::sort( sid.begin(), sid.end(),
      []( const InputLayoutDesc& lhs, const InputLayoutDesc& rhs ) -> bool {
        return lhs.input_buffer_slot < rhs.input_buffer_slot;
      }
    );

    bind();

    int32_t bound_input_slot = -1;
    int32_t attrib_location = 0;

    for ( const auto& layout : sid ) {
      
      int32_t input_slot = (int32_t)layout.input_buffer_slot;

      if ( bound_input_slot != input_slot ) {
        buffers[input_slot]->bind();
        bound_input_slot = input_slot;
      }

      uint32_t attrib_index = sp->get_attrib_location( layout.semantic_name );

      glVertexAttribPointer( (GLuint)attrib_index, (GLint)get_num_elements( layout.data_type ), map( layout.data_type ), false, (GLsizei)buffers[input_slot]->stride(), (const void*)layout.offset );
      glEnableVertexAttribArray( (GLuint)attrib_index );
    }

    _buffers.insert( _buffers.end(), buffers.begin(), buffers.end() );

    EG_CORE_DEBUG( "created opengl vertex array with id = {}", _vao );
  }

  OpenGLVertexArray::~OpenGLVertexArray( void ) {
    glDeleteVertexArrays( 1, &_vao );
    EG_CORE_DEBUG( "deleted opengl vertex array with id = {}", _vao );
  }

  void OpenGLVertexArray::bind( void ) const {
    glBindVertexArray( _vao );
  }

}

#endif /* EG_GAPI_OPENGL */
