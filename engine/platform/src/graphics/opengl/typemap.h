#pragma once

#include <glad/glad.h>

#include <platform/graphics/buffers.h>
#include <platform/graphics/inputlayout.h>
#include <platform/graphics/renderstate.h>

namespace eg {

  static GLenum map( BufferType type ) {
		switch ( type ) {
		case eg::BufferType::ARRAY:   return GL_ARRAY_BUFFER;
		case eg::BufferType::INDEX:   return GL_ELEMENT_ARRAY_BUFFER;
		case eg::BufferType::UNIFORM:	return GL_UNIFORM_BUFFER;
		case eg::BufferType::TEXTURE: return GL_TEXTURE_BUFFER;
		default:
			break;
		}

		EG_THROW( "invalid buffer type" );
		return GL_INVALID_ENUM;
  }

	static GLenum map( BufferUsage usage, BufferAccess access ) {
		switch (usage) {
		case eg::BufferUsage::STATIC:
			switch ( access ) {
			case BufferAccess::DRAW: return GL_STATIC_DRAW;
			case BufferAccess::READ: return GL_STATIC_READ;
			case BufferAccess::COPY: return GL_STATIC_COPY;
			default: break;
			}
			break;
		case eg::BufferUsage::STREAM:
			switch (access) {
			case BufferAccess::DRAW: return GL_STREAM_DRAW;
			case BufferAccess::READ: return GL_STREAM_READ;
			case BufferAccess::COPY: return GL_STREAM_COPY;
			default: break;
			}
			break;
		case eg::BufferUsage::DYNAMIC:
			switch (access) {
			case BufferAccess::DRAW: return GL_DYNAMIC_DRAW;
			case BufferAccess::READ: return GL_DYNAMIC_READ;
			case BufferAccess::COPY: return GL_DYNAMIC_COPY;
			default: break;
			}
			break;
		default:
			break;
		}

		EG_THROW( "invalid buffer usage" );
		return GL_INVALID_ENUM;
	}

	static GLenum map( DataType type ) {
		switch (type) {
		case eg::DataType::INT32:
		case eg::DataType::INT32_2:
		case eg::DataType::INT32_3:
		case eg::DataType::INT32_4:   return GL_INT;
		case eg::DataType::FLOAT32:
		case eg::DataType::FLOAT32_2:
		case eg::DataType::FLOAT32_3:
		case eg::DataType::FLOAT32_4: return GL_FLOAT;
		default:
			break;
		}

		EG_THROW( "invalid data type" );
		return GL_INVALID_ENUM;
	}

	static GLenum map( Topology topology ) {
		switch (topology) {
		case eg::Topology::INVALID:            return GL_INVALID_ENUM;
		case eg::Topology::POINTS:             return GL_POINTS;
		case eg::Topology::LINES:              return GL_LINES;
		case eg::Topology::LINE_STRIP:         return GL_LINE_STRIP;
		case eg::Topology::LINES_ADJ:          return GL_LINES_ADJACENCY;
		case eg::Topology::LINE_STRIP_ADJ:     return GL_LINE_STRIP_ADJACENCY;
		case eg::Topology::TRIANGLES:			     return GL_TRIANGLES;
		case eg::Topology::TRIANGLE_STRIP:     return GL_TRIANGLE_STRIP;
		case eg::Topology::TRIANGLES_ADJ:      return GL_TRIANGLES_ADJACENCY;
		case eg::Topology::TRIANGLE_STRIP_ADJ: return GL_TRIANGLE_STRIP_ADJACENCY;
		default:
			break;
		}

		EG_THROW("invalid primitive topology");
		return GL_INVALID_ENUM;
	}

}
