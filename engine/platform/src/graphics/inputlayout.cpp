#include <platform/graphics/inputlayout.h>

namespace eg {

  Ref<InputLayout> InputLayout::create( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers, const GraphicsContext* context ) {

    EG_ASSERT( context );

    InputLayout* val = nullptr;
    GraphicsAPI  api = context->get_api();

    DEFINE_OBJECT_CREATE_FUNCTION( api, create, shader_program, input_desc, buffers, context );

    if ( !val )
      EG_THROW( "failed to create input layout" );

    return Ref<InputLayout>( val );
  }

  Ref<InputLayout> InputLayout::create( const Ref<ShaderProgram>& shader_program, const std::vector<InputLayoutDesc>& input_desc, const std::vector<Ref<VertexBuffer>>& buffers ) {
    return create( shader_program, input_desc, buffers, GraphicsContext::get() );
  }

}