#include <platform/graphics/buffers.h>

namespace eg {

  Ref<Buffer> Buffer::create( const BufferCreateInfo& create_info, BufferType type, const GraphicsContext* context ) {
    
    EG_ASSERT( context );

    Ref<Buffer> val;

    switch ( type ) {
    case BufferType::ARRAY:
      val = VertexBuffer::create( create_info, context );
      break;
    case BufferType::INDEX:
    case BufferType::UNIFORM:
    case BufferType::TEXTURE:
      EG_THROW( "buffer type not yet implmented" );
      break;
    default:
      EG_THROW( "invalid buffer type" );
      break;
    }

    return val;
  }

  Ref<Buffer> Buffer::create( const BufferCreateInfo& create_info, BufferType type ) {
    return create( create_info, type, GraphicsContext::get() );
  }

  Ref<VertexBuffer> VertexBuffer::create( const BufferCreateInfo& create_info, const GraphicsContext* context ) {
    
    EG_ASSERT( context );

    VertexBuffer* val = nullptr;
    GraphicsAPI   api = context->get_api();

    DEFINE_OBJECT_CREATE_FUNCTION( api, create, create_info, context );

    if ( !val )
      EG_THROW( "failed to create vertex buffer" );

    return Ref<VertexBuffer>( val );
  }

  Ref<VertexBuffer> VertexBuffer::create( const BufferCreateInfo& create_info ) {
    return create( create_info, GraphicsContext::get() );
  }

}