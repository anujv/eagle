#pragma once

/* Simple wrapper to create glm abstraction, */
/* also, this makes it easier to replace glm */
/* later with some other library.            */

#include <glm/glm.hpp>

namespace eg {

  using vec2i  = glm::ivec2;
  using vec3i  = glm::ivec3;
  using vec4i  = glm::ivec4;
  
  using vec2   = glm::vec2;
  using vec3   = glm::vec3;
  using vec4   = glm::vec4;
  
  using mat3   = glm::mat3;
  using mat4   = glm::mat4;

}
