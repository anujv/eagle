#pragma once

#include <base/event.h>
#include <base/ref.h>

namespace eg {

  class Module : public SharedObject {
    friend class ModuleManager;
  protected:
    Module( string name ) : _name( name ) {}
    virtual ~Module( void ) {}
  protected:
    virtual void startup( void )               = 0;
    virtual void shutdown( void )              = 0;
  public:
    virtual void on_event( Event& e )          = 0;
    virtual void on_update( float delta_time ) = 0;
    virtual void pre_update( void )            = 0;
    virtual void post_update( void )           = 0;

    string name() const { return _name; }
  private:
    string _name;
  };

}
