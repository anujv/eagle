#pragma once

#include <base/types.h>
#include <platform/graphics/api.h>

#include "math.h"

namespace eg {

  struct CoreConfig {
    vec2i       window_dim;
    string      window_name;
    GraphicsAPI gfx_api;
  };

}
