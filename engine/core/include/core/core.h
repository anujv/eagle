#pragma once

#include <base/singleton.h>
#include <base/event.h>

#include <core/modmanager.h>

#include "config.h"
#include "modmanager.h"

namespace eg {

  class Core : public ModuleManager, public Singleton<Core> {
    REGISTER_SINGLETON_CLASS( Core );
  protected:
    Core( CoreConfig config );
    ~Core( void );
  public:
    void run( void );
  private:
    void event( Event& e );
    void update( void );
    void pre_update( void );
    void post_update( void );
  private:
    bool _should_close;
  };

}
