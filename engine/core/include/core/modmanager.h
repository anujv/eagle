#pragma once

#include <unordered_map>
#include <functional>

#include <base/ref.h>

#include <core/module.h>

namespace eg {

  using module_iterator_func = std::function<void( Ref<Module>, void* )>;

  class ModuleManager {
  protected:
    ModuleManager( void ) {}
    virtual ~ModuleManager( void ) {}
  public:
    void add_module( Ref<Module> mod );
    void remove_module( Ref<Module> mod );
    void remove_module( string name );

    void iterate_modules( module_iterator_func func, void* user );

    Ref<Module> get_module( string name ) const;
  private:
    std::unordered_map<string, Ref<Module>> _modules;
  };

}
