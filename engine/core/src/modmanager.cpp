#include <core/modmanager.h>

#include <platform/common/log.h>

namespace eg {

  void ModuleManager::add_module( Ref<Module> mod ) {
    if ( !mod ) { EG_CORE_ERROR( "failed to add invalid module" ); }

    auto it = _modules.find( mod->name() );

    if ( it != _modules.end() ) {
      EG_CORE_ERROR( "a module with name '{}' already exists in the map; failed to add module", mod->name() );
      return;
    }

    _modules.insert( std::make_pair( mod->name(), mod ) );
    mod->startup();
  }

  void ModuleManager::remove_module( Ref<Module> mod ) {
    if ( !mod ) {
      EG_CORE_ERROR( "trying to remove invalid module" );
      return;
    }

    auto it = _modules.find( mod->name() );

    if ( it != _modules.end() ) {
      it->second->shutdown();
      _modules.erase( it );
      return;
    }

    EG_CORE_ERROR( "failed to remove module with name '{}'; module not found", mod->name() );
  }
  
  void ModuleManager::remove_module( string name ) {
    auto it = _modules.find( name );

    if ( it != _modules.end() ) {
      it->second->shutdown();
      _modules.erase( it );
      return;
    }

    EG_CORE_ERROR( "failed to remove module with name '{}'; module not found", name );
  }

  void ModuleManager::iterate_modules( module_iterator_func func, void* user ) {
    if ( func ) {
      for ( auto& mod : _modules ) {
        func( mod.second, user );
      }
    }
  }

  Ref<Module> ModuleManager::get_module( string name ) const {
    auto it = _modules.find( name );

    if ( it != _modules.end() )
      return it->second;
    else {
      EG_CORE_ERROR( "failed to find module with name '{}'", name );
      return nullptr;
    }
  }

}
