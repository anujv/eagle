#include <core/core.h>

#include <platform/events/platform_events.h>
#include <platform/common/window.h>
#include <platform/common/log.h>

namespace eg {

  #define EG_BIND_EVENT_FN(fn) [this](auto&&... args) -> decltype(auto) { return this->fn(std::forward<decltype(args)>(args)...); }

  Core::Core( CoreConfig config ) : _should_close( false ) {
    Log::init( "any:6" );

    WindowCreateInfo window_create_info = {};
    window_create_info.x_dim            = config.window_dim.x;
    window_create_info.y_dim            = config.window_dim.y;
    window_create_info.name             = config.window_name;
    window_create_info.api              = config.gfx_api;

    Window::create_instance( window_create_info );

    Window::ref().bind_event_function( EG_BIND_EVENT_FN( Core::event ) );
  }

  Core::~Core( void ) {
    Window::destory_instance();
  }

  void Core::run( void ) {

    while ( !_should_close ) {
      pre_update();
      update();
      post_update();

      Window::ref().poll_events();
    }

  }

  void Core::event( Event& e ) {
    if ( e.type() == eg::WINDOW_CLOSE_EVENT )
      _should_close = true;

    iterate_modules( [&]( Ref<Module> mod, void* user ) -> void {
      mod->on_event( e );
    }, nullptr );
  }

  void Core::update( void ) {
    iterate_modules( []( Ref<Module> mod, void* user ) -> void {
      mod->on_update( 0.0f );
    }, nullptr );
  }

  void Core::pre_update( void ) {
    iterate_modules( []( Ref<Module> mod, void* user ) -> void {
      mod->pre_update();
    }, nullptr );
  }
  
  void Core::post_update( void ) {
    iterate_modules( []( Ref<Module> mod, void* user ) -> void {
      mod->post_update();
    }, nullptr );
  }

}
