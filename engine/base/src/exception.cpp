#include <base/exception.h>

#include <cstring>

namespace eg {

  EGException::EGException( const string& message, const string& file, uint32_t line ) :
    _message( message ),
    _file( file ),
    _line( line ) {}

  string EGException::trimmed_file( void ) const {
    size_t slash_location = _file.find_last_of( '\\' );
    if ( slash_location == string::npos )
      slash_location = _file.find_last_of( '/' );

    if ( slash_location != string::npos )
      return _file.substr( slash_location + 1 );

    return _file;
  }

  EGShaderCompilationException::EGShaderCompilationException( const string& log, const string& message, const string& file, uint32_t line ) :
    EGException( message, file, line ), _log( log ) {}

}