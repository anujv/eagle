#pragma once

#include <string>
#include <cstdint>

typedef float  egfloat;  /* 4-byte floating point number */

typedef double egdouble; /* 8-byte floating point number */

namespace eg {
  using string = std::string;
}
