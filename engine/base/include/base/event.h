#pragma once

#include <functional>

#include "types.h"

namespace eg {

  class Event;

  typedef int32_t                         event_type;
  typedef std::function<void( Event& e )> event_emit_func;

  class Event {
  protected:
    Event() {}
    virtual ~Event() {}
  public:
    virtual event_type type( void ) const = 0;
    virtual string     name( void ) const = 0;
  };

  #define REGISTER_EVENT( E )                                              \
            virtual event_type type(  void ) const override { return  E; } \
            virtual string     name(  void ) const override { return #E; } \
            static  event_type static_type( void ) { return E; }

}
