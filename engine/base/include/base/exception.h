#pragma once

#include <exception>
#include <stdexcept>

#include "types.h"

namespace eg {

  class EGException {
  public:
    EGException( const string& message, const string& file, uint32_t line );
    ~EGException( void ) = default;
  public:
    string          trimmed_file( void ) const;

    inline string   what( void ) const { return _message; }
    inline string   file( void ) const { return _file; }
    inline uint32_t line( void ) const { return _line; }
  private:
    string   _file;
    string   _message;
    uint32_t _line;
  };

  class EGShaderCompilationException : public EGException {
  public:
    EGShaderCompilationException( const string& log, const string& message, const string& file, uint32_t line );
    ~EGShaderCompilationException( void ) = default;
  public:
    inline string log( void ) const { return _log; }
  private:
    string _log;
  };

}

#define EG_THROW( M )                                     \
        do {                                              \
          throw eg::EGException( M, __FILE__, __LINE__ ); \
        } while ( 0 );

#define EG_THROW_SPECIFIC( T, ... )                       \
        do {                                              \
          throw eg::T( __VA_ARGS__, __FILE__, __LINE__ ); \
        } while ( 0 );
