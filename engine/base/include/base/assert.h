#pragma once

#include <cassert>

#define EG_ASSERT( X ) assert( X )