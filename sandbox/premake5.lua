project "sandbox"

  kind          "ConsoleApp"
  language      "C++"
  cppdialect    "C++17"
  staticruntime "on"

  targetdir( build_dir .. "/" .. output_dir .. "/bin/%{prj.name}" )
  objdir( build_dir .. "/" .. output_dir .. "/bin-int/%{prj.name}" )

  files {
    "src/**.h",
    "src/**.cpp",
    "include/**.h"
  }

  includedirs {
    "src",
    "include",
    include_dirs["base"],
    include_dirs["platform"],
    include_dirs["core"],
    common_include_dirs
  }

  links {
    "core"
  }

  filter "configurations:debug*"
    defines {
      "EG_DEBUG",
      "DEBUG"
    }
    runtime "Debug"
    symbols "On"

  filter "configurations:release*"
    defines {
      "EG_RELEASE",
      "NDEBUG",
    }
    runtime "Release"
    optimize "On"


  -- cleanup --

  if _ACTION == "clean" then
    os.remove( "sandbox.vcxproj" )
    os.remove( "sandbox.vcxproj.filters" )
    os.remove( "sandbox.vcxproj.user" )
  end