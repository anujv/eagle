
#include <platform/common/log.h>
#include <platform/common/window.h>
#include <platform/events/platform_events.h>
#include <platform/events/keyboard_events.h>

#include <platform/graphics/context.h>
#include <platform/graphics/shaders.h>
#include <platform/graphics/buffers.h>
#include <platform/graphics/inputlayout.h>
#include <platform/graphics/renderstate.h>
#include <platform/graphics/imguicontext.h>

#include <base/exception.h>

#include <entt/entt.hpp>

#include <core/core.h>
#include <modules/modules.h>

bool should_close = false;

void event_func( eg::Event& e ) {

  if ( e.type() == eg::WINDOW_CLOSE_EVENT ) {
    should_close = true;
  } else {
    EG_LOG_INFO( "unknown event - {}", e.name() );
  }

}

char vsource[] = R"(
  #version 460 core

  layout ( location = 1 ) in vec3 position;
  layout ( location = 0 ) in vec4 color;

  out vec4 pass_color;

  void main( void ) {
    pass_color = color;
    gl_Position = vec4( position, 1.0f );
  }

)";

char dvsource[] = R"(
  
  struct VSOut {
    float4 out_pos : SV_POSITION;
    float4 color   : PASS_COLOR;
  };

  VSOut main( float3 pos : position, float4 col : color ) {
    VSOut vso;
    vso.out_pos = float4( pos.x, pos.y, pos.z, 1.0f );
    vso.color   = col;
    return vso;
  }

)";

char psource[] = R"(
  #version 460 core

  out vec4 frag_color;

  in  vec4 pass_color;

  void main( void ) {
    frag_color = pass_color;
  }

)";

char dpsource[] = R"(
  
  struct VSIn {
    float4 out_pos : SV_POSITION;
    float4 color   : PASS_COLOR;
  };

  float4 main( VSIn vsi ) : SV_TARGET {
    float4 out_col;

    out_col = vsi.color;

    return out_col;
  }

)";

float vertices[] = {
   0.0f,  0.5f, 0.0f,
   0.5f, -0.5f, 0.0f,
  -0.5f, -0.5f, 0.0f
};

float colors[] = {
  1.0f, 0.0f, 0.0f, 1.0f,
  0.0f, 1.0f, 0.0f, 1.0f,
  0.0f, 0.0f, 1.0f, 1.0f,
};

class mod : public eg::Module {
public:
  mod() : Module( "mod" ) {}
  ~mod() {}
public:
  virtual void startup( void ) {
    using namespace eg;

    eg::ImGuiContext::create_instance(GraphicsContext::get(), Window::get());

    Viewport vp = {};
    vp.top_left_x = 0;
    vp.top_left_y = 0;
    vp.width = 800;
    vp.height = 600;
    vp.min_depth = 0.0f;
    vp.max_depth = 0.0f;

    RenderState::ref().push_topology(Topology::TRIANGLES);
    RenderState::ref().push_viewport(vp);

    Ref<Shader> vs;
    Ref<Shader> ps;

    if (GraphicsContext::ref().get_api() == GraphicsAPI::DIRECTX) {
      vs = VertexShader::create("gg", dvsource);
      ps = PixelShader::create("wp", dpsource);
    }
    else {
      vs = VertexShader::create("gg", vsource);
      ps = PixelShader::create("wp", psource);
    }

    sp = ShaderProgram::create(vs, ps);

    BufferCreateInfo create_info = {};
    create_info.data = vertices;
    create_info.size = sizeof(vertices);
    create_info.usage = BufferUsage::STATIC;
    create_info.access = BufferAccess::DRAW;
    create_info.stride = sizeof(float) * 3;

    Ref<VertexBuffer> buf = VertexBuffer::create(create_info);

    create_info.data = colors;
    create_info.size = sizeof(colors);
    create_info.stride = sizeof(float) * 4;

    Ref<VertexBuffer> bufc = VertexBuffer::create(create_info);

    std::vector<InputLayoutDesc> input_layout;
    input_layout.push_back({ "position", DataType::FLOAT32_3, 0, 0 });
    input_layout.push_back({ "color",    DataType::FLOAT32_4, 1, 0 });

    std::vector<Ref<VertexBuffer>> buffers{ buf, bufc };

    layout = InputLayout::create(sp, input_layout, buffers);
  }
  virtual void shutdown( void ) {
    sp = nullptr;
    layout = nullptr;
  }
  virtual void on_event( eg::Event& e ) {}
  virtual void on_update( float delta_time ) {
    layout->bind();
    sp->bind();

    eg::RenderState::ref().draw(3);

    eg::ImGuiContext::ref().begin_frame();

    ImGui::ShowDemoWindow();

    eg::ImGuiContext::ref().end_frame();
  }
  virtual void pre_update( void ) {}
  virtual void post_update( void ) {}
private:
  eg::Ref<eg::ShaderProgram> sp;
  eg::Ref<eg::InputLayout> layout;
};

int main() {

  using namespace eg;

  try {
    CoreConfig config  = {};
    config.window_dim  = vec2i( 800, 600 );
    config.window_name = "Donkey Fart Box";
    config.gfx_api     = GraphicsAPI::DIRECTX;

    Core::create_instance( config );

    Modules::create_instance();
    Modules::ref().register_modules();

    Core::ref().add_module( new mod() );

    Core::ref().run();

    Modules::ref().deregister_modules();
    Modules::destory_instance();

    Core::destory_instance();

  } catch ( const EGShaderCompilationException& e ) {
    EG_LOG_ERROR( "shader compilation error - {}", e.log() );
  } catch ( const EGException& e ) {
    EG_LOG_CRITICAL( "generic runtime exception - {} @ [{}, {}]", e.what(), e.trimmed_file(), e.line() );
  }

  return 0;
}
